package ru.freshtape.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.util.Pair;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.freshtape.activities.ViewPostActivity;
import ru.freshtape.model.Author;
import ru.freshtape.model.CustomItemPost;
import ru.freshtape.model.ModelAnswerVideo;
import ru.freshtape.model.ModelPost;
import ru.freshtape.model.ModelPostWithAuthor;
import ru.freshtape.model.ModelProgressBar;
import ru.freshtape.model.SmallModelPost;
import ru.freshtape.model.Types;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.vk.FreshTape;
import ru.freshtape.R;
import ru.freshtape.dialog.DialogViewGIF;
import ru.freshtape.vk.VkApiHelper;

/**
 * Created by Павел on 10.05.2017.
 */

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CustomItemPost> items;
    private TreeSet<Pair<Long, Long>> itemsSet;
    private boolean isPB;
    private View progressBar;
    private Activity activity;
    private boolean isEnd;
    private final int LIMIT_TEXT_POST = 150;
    private boolean isAllView, isColorScanned;
    private FreshTape freshTape;
    private int size_w, size_h;

    public PostAdapter(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        size_w = (int) (((float) size.x) * 0.9f);
        size_h = (int) (((float) size.y) * 0.9f);

        items = new ArrayList<>();
        itemsSet = new TreeSet<>(new Comparator<Pair<Long, Long>>() {
            public int compare(Pair<Long, Long> a, Pair<Long, Long> b) {
                if (a.first.equals(b.first))
                    return (int) (b.second - a.second);
                return (int) (b.first - a.first);
            }
        });
        isPB = false;
        isEnd = false;
        this.activity = activity;
        progressBar = null;
        isAllView = false;
    }

    public void setTapeHelper(FreshTape freshTape) {
        this.freshTape = freshTape;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Types.TYPE_WITH_AUTHOR_POST) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.model_item_post, parent, false);


            TextView alarmTV = (TextView) v.findViewById(R.id.alarmTV);
            alarmTV.setVisibility(View.GONE);

            AppCompatImageView openPost = (AppCompatImageView) v.findViewById(R.id.openPost);

            TextView viewCounter = (TextView) v.findViewById(R.id.viewCounter),
                    repostCounter = (TextView) v.findViewById(R.id.repostCounter),
                    commentCounter = (TextView) v.findViewById(R.id.commentCounter),
                    likeCounter = (TextView) v.findViewById(R.id.likeCounter);

            CardView postCV = (CardView) v.findViewById(R.id.postCV);

            ModelPostView mainPostView = new ModelPostView(v.findViewById(R.id.mainPostView));
            ModelPostView historyPostView = new ModelPostView(v.findViewById(R.id.historyPostView));
            return new ItemPostHolder(v, postCV, openPost, viewCounter, repostCounter, commentCounter, likeCounter,
                    (LinearLayout) v.findViewById(R.id.historyLL),
                    mainPostView, historyPostView);

        } else if (viewType == Types.TYPE_PROGERSS_BAR) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.model_progres_bar, parent, false);

            progressBar = v;

            ProgressBar modelPB = (ProgressBar) v.findViewById(R.id.modelPB);

            return new ItemProgressBarHolder(v, modelPB);
        }
        return null;
    }

    private String formatCounter(long count) {
        if (count > 1000L) {
            return String.valueOf(count / 1000) + "K";
        } else {
            return String.valueOf(count);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final CustomItemPost customItem = items.get(position);

        if (customItem.getTypeView() == Types.TYPE_WITH_AUTHOR_POST) {

            final ModelPostWithAuthor postWithAuthor = (ModelPostWithAuthor) customItem;
            ItemPostHolder itemHolder = (ItemPostHolder) holder;

            if (postWithAuthor.isView() && isColorScanned) {
                itemHolder.postCV.setCardBackgroundColor(ContextCompat.getColor(itemHolder.itemView.getContext(), R.color.gray));
            } else {
                itemHolder.postCV.setCardBackgroundColor(ContextCompat.getColor(itemHolder.itemView.getContext(), R.color.text_color_2));
            }

            View.OnClickListener listenerOpenAllView = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ViewPostActivity.class);
                    intent.putExtra(PreferencesHelper.FROM_ID, postWithAuthor.getFrom_id());
                    intent.putExtra(PreferencesHelper.ID_POST, postWithAuthor.getWall().getId());
                    activity.startActivity(intent);
                }
            };

            if (!isAllView) {
                itemHolder.itemView.setOnClickListener(listenerOpenAllView);
            }

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getId() == R.id.openPost) {
                        activity.startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://vk.com/wall" + postWithAuthor.getFrom_id() + "_" + postWithAuthor.getId())));
                    }
                }
            };

            itemHolder.openPost.setOnClickListener(listener);

            itemHolder.likeCounter.setText(formatCounter(postWithAuthor.getWall().getLikes().getCount()));
            itemHolder.commentCounter.setText(formatCounter(postWithAuthor.getWall().getComments().getCount()));
            itemHolder.repostCounter.setText(formatCounter(postWithAuthor.getWall().getReposts().getCount()));
            if (postWithAuthor.getWall().getViews() != null) {
                itemHolder.viewCounter.setText(formatCounter(postWithAuthor.getWall().getViews().getCount()));
            } else {
                itemHolder.viewCounter.setText("---");
            }

            itemHolder.mainPostView.postView.setVisibility(View.VISIBLE);
            createViewPost(itemHolder.mainPostView, postWithAuthor.getWall(), postWithAuthor.getAuthor());

            if (postWithAuthor.getWall().isHaveHistory()) {
                if (!isAllView)
                    itemHolder.itemView.setOnClickListener(listenerOpenAllView);
                itemHolder.historyLL.setVisibility(View.VISIBLE);
                createViewPost(itemHolder.historyPostView, postWithAuthor.getWall().getCopy_history()[0], postWithAuthor.getHistoryAuthor());
            } else {
                itemHolder.historyLL.setVisibility(View.GONE);
            }


            if (freshTape != null && !isAllView) {
                freshTape.viewed(new SmallModelPost(postWithAuthor.getWall().getFrom_id(),
                        postWithAuthor.getWall().getDate(),
                        postWithAuthor.getWall().getId()));
                postWithAuthor.setView(true);
            }

        } else if (customItem.getTypeView() == Types.TYPE_PROGERSS_BAR) {
            ItemProgressBarHolder itemHolder = (ItemProgressBarHolder) holder;

            if (isEnd)
                itemHolder.modelPB.setVisibility(View.GONE);
            else {
                itemHolder.modelPB.setVisibility(View.VISIBLE);
            }
        }
    }

    private void createViewPost(ModelPostView modelPostView, ModelPost modelPost, Author author) {

        modelPostView.removeAllViews();

        modelPostView.alarmTV.setVisibility(View.GONE);
        modelPostView.linkTV.setVisibility(View.GONE);
        modelPostView.pageTV.setVisibility(View.GONE);

        if (modelPost.getText() != null && modelPost.getText().length() >= LIMIT_TEXT_POST && !isAllView) {
            String str = modelPost.getText().substring(0, LIMIT_TEXT_POST - 1)
                    + activity.getResources().getString(R.string.more_text);

            //str = createText(str);

            modelPostView.textTV.setText(str);
            modelPostView.textTV.setLinksClickable(false);
            Linkify.addLinks(modelPostView.textTV, 0);

            modelPostView.textTV.setTextIsSelectable(false);
            modelPostView.textTV.setMovementMethod(null);
        } else {
            modelPostView.textTV.setTextIsSelectable(true);
            String str = modelPost.getText();

            str = createText(str);

            modelPostView.textTV.setText(str);
            modelPostView.textTV.setMovementMethod(LinkMovementMethod.getInstance());
            //modelPostView.textTV.setText(createText(modelPost.getText()), TextView.BufferType.SPANNABLE);
            modelPostView.textTV.setLinksClickable(true);
            Linkify.addLinks(modelPostView.textTV, Linkify.WEB_URLS);
        }

        if (modelPost.getIs_pinned() == 1)
            modelPostView.fixedTV.setVisibility(View.VISIBLE);
        else
            modelPostView.fixedTV.setVisibility(View.GONE);

        author.getName();
        modelPostView.nameTV.setText(author.getName());


        long date = modelPost.getDate() * 1000;
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT);
        modelPostView.dateTV.setText(df.format(date));

        Picasso.with(activity)
                .load(author.getPhoto_50())
                .fit()
                .placeholder(R.drawable.circle)
                .error(R.drawable.circle)
                .into(modelPostView.authorPhotoIV);

        if (modelPost.getAttachments() != null) {
            List<String> listPhotos = new ArrayList<>();
            int counter = 0;
            for (ModelPost.Attachments i : modelPost.getAttachments()) {
                switch (i.getType()) {
                    case Types.type_photo:
                        counter++;
                        break;
                    case Types.type_video:
                        counter++;
                        break;
                    case Types.type_doc:
                        counter++;
                        break;
                }
            }
            int WH = (int)((float)size_w / 3f);
            if (counter == 1)
                WH = size_w;
            else if (counter <= 4)
                WH = (int)((float)size_w / 2f);

            for (ModelPost.Attachments i : modelPost.getAttachments()) {
                switch (i.getType()) {
                    case Types.type_photo:
                        createPhotosPostImgs(modelPostView.addPostImgs(counter), i, listPhotos, WH);
                        break;
                    case Types.type_video:
                        createVideoPostImgs(modelPostView.addPostImgs(counter), i, WH);
                        break;
                    case Types.type_doc:
                        createDocPostImgs(modelPostView.addPostImgs(counter), i, WH);
                        break;
                    case Types.type_link:
                        createLinkPost(modelPostView.linkTV, i.getLink());
                        break;
                    case Types.type_page:
                        createPagePost(modelPostView.pageTV, i.getPage());
                        break;
                    default:
                        modelPostView.alarmTV.setText(activity.getResources().getString(R.string.alarm_post) + " " + i.getType());
                        modelPostView.alarmTV.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void createPagePost(TextView pageTV, final ModelPost.Attachments.Page page) {
        pageTV.setVisibility(View.VISIBLE);

        String s = String.format(activity.getString(R.string.page), page.getTitle());

        SpannableString ss = new SpannableString(s);
        ss.setSpan(new UnderlineSpan(), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        pageTV.setText(ss);
        pageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(page.getView_url()));
                activity.startActivity(intent);
            }
        });
    }

    private void createLinkPost(TextView linkTV, final ModelPost.Attachments.Link link) {
        linkTV.setVisibility(View.VISIBLE);

        String s = String.format(activity.getString(R.string.link), link.getTitle());

        SpannableString ss = new SpannableString(s);
        ss.setSpan(new UnderlineSpan(), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        linkTV.setText(ss);
        linkTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getUrl()));
                activity.startActivity(intent);
            }
        });
    }

    private String createText(String str) {
        int indexBegin = str.indexOf('['),
                indexSep = str.indexOf('|'),
                indexEnd = str.indexOf(']');
//String res = str;
        while (indexBegin != -1 && indexSep != -1 && indexEnd != -1
                && (indexBegin + 1) != indexSep
                && (indexSep + 1) != indexEnd
                && indexBegin < indexSep && indexSep < indexEnd) {

            str = str.replace(str.substring(indexBegin, indexEnd + 1), "https://vk.com/" + str.substring(indexBegin + 1, indexSep));

            //String linkHRef = str.substring(indexBegin + 1, indexSep),
            //        linkText = str.substring(indexSep + 1, indexEnd);
            //String htmlLink = "<a href='%s'>%s</a>";
            //Spanned spanned = Html.fromHtml(String.format(htmlLink, linkHRef, linkText));

            indexBegin = str.indexOf('[');
            indexSep = str.indexOf('|');
            indexEnd = str.indexOf(']');

            //return spanned;
        }
        return str;
    }

    private void setImg(String url, AppCompatImageView imageView, int w, int h) {
        if (url == null || url.isEmpty())
            return;
        imageView.getLayoutParams().width = w;
        imageView.getLayoutParams().height = h;
        imageView.requestLayout();
        Picasso.with(activity)
                .load(url)
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_STORE, NetworkPolicy.NO_CACHE)
                .fit()
                .placeholder(AppCompatDrawableManager.get().getDrawable(activity.getApplicationContext(), R.drawable.ic_more_horiz))
                .error(AppCompatDrawableManager.get().getDrawable(activity.getApplicationContext(), R.drawable.ic_error_outline))
                .into(imageView);

    }

    private void createPhotosPostImgs(PostImg postImg, ModelPost.Attachments i, final List<String> listPhotos, int width_l) {
        listPhotos.add(i.getPhoto().getMaxSizePhoto());
        final int indexPhoto = listPhotos.size() - 1;
        View.OnClickListener listenerImg = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPhoto(listPhotos, indexPhoto);
            }
        };

        int height_l = (int)(((float)width_l * (float)i.getPhoto().getHeight()) / (float)i.getPhoto().getWidth());

        postImg.title.setText(null);
        postImg.img.setOnClickListener(listenerImg);
        setImg(i.getPhoto().getPhoto_604(), postImg.img, width_l, height_l);
        postImg.img.setVisibility(View.VISIBLE);
        postImg.title.setVisibility(View.INVISIBLE);
    }

    private void createDocPostImgs(PostImg postImg, final ModelPost.Attachments i, int width_l) {
        View.OnClickListener listenerGIF;
        if (i.getDoc().getType() == Types.TypesDoc.TYPE_GIF) {

            int height_l = (int)(((float)width_l *
                    (float)i.getDoc().getPreview().getPhoto().getSizes()[0].getHeight())
                    / (float)i.getDoc().getPreview().getPhoto().getSizes()[0].getWidth());

            setImg(i.getDoc().getPreview().getPhoto().getSizes()[0].getSrc(), postImg.img, width_l, height_l);
            listenerGIF = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (i.getDoc().getPreview().getVideo() != null) {
                        DialogViewGIF dialogViewGIF = new DialogViewGIF();
                        dialogViewGIF.setUrl(i.getDoc().getPreview().getVideo().getSrc());
                        dialogViewGIF.show(activity.getFragmentManager(), "AddInFavorite");
                    } else {
                        Toast.makeText(activity.getApplicationContext(), R.string.warning, Toast.LENGTH_LONG).show();
                    }
                }
            };
        } else {
            listenerGIF = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                                    /*DownloadManager.Request r = new DownloadManager.Request(Uri.parse(i.getDoc().getUrl()));
                                    // This put the download in the same Download dir the browser uses
                                    r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "fileName");
                                    // When downloading music and videos they will be listed in the player
                                    // (Seems to be available since Honeycomb only)
                                    r.allowScanningByMediaScanner();
                                    // Notify user when download is completed
                                    // (Seems to be available since Honeycomb only)
                                    r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                    // Start download
                                    DownloadManager dm = (DownloadManager) activity.getSystemService(DOWNLOAD_SERVICE);
                                    dm.enqueue(r);*/

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(i.getDoc().getUrl()));
                    activity.startActivity(intent);
                }
            };
        }
        postImg.img.setOnClickListener(listenerGIF);

        if (i.getDoc().getTitle() != null && i.getDoc().getTitle().length() <= 20)
            postImg.title.setText(i.getDoc().getTitle());
        else if (i.getDoc().getTitle() != null)
            postImg.title.setText(i.getDoc().getTitle().substring(0, 20 - 1)
                    + activity.getResources().getString(R.string.more_text));

        postImg.title.setVisibility(View.VISIBLE);
        postImg.img.setImageDrawable(AppCompatDrawableManager.get().getDrawable(activity, R.drawable.downward_ic));
        postImg.img.setScaleType(ImageView.ScaleType.FIT_XY);
        postImg.img.requestLayout();
        postImg.img.setVisibility(View.VISIBLE);
        postImg.title.setVisibility(View.VISIBLE);
    }

    private void createVideoPostImgs(PostImg postImg, final ModelPost.Attachments i, int width_l) {
        View.OnClickListener listenerVideo = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VkApiHelper.getVideo(
                        i.getVideo().getOwner_id() + "_" + i.getVideo().getId() + "_" + i.getVideo().getAccess_key(),
                        new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                GsonBuilder builder = new GsonBuilder();
                                Gson gson = builder.create();
                                ModelAnswerVideo answer = gson.fromJson(response.json.toString(), ModelAnswerVideo.class);

                                Uri uri;
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                uri = Uri.parse(answer.getResponse().getItems()[0].getPlayer());
                                intent.setData(uri);

                                /*if (answer.getResponse().getItems()[0].getFiles() == null
                                        || answer.getResponse().getItems()[0].getFiles().getMp4_720() == null
                                        || answer.getResponse().getItems()[0].getFiles().getMp4_720().isEmpty()) {
                                    uri = Uri.parse(answer.getResponse().getItems()[0].getPlayer());
                                    intent.setData(uri);
                                } else {
                                    uri = Uri.parse(answer.getResponse().getItems()[0].getFiles().getMp4_720());
                                    intent.setDataAndType(uri, "video/mp4");
                                }*/

                                activity.startActivity(intent);
                            }

                            @Override
                            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                                super.attemptFailed(request, attemptNumber, totalAttempts);
                            }

                            @Override
                            public void onError(VKError error) {
                                super.onError(error);
                            }
                        });

            }
        };


        int height_l = (int)(((float)width_l * (float)240) / (float)320);

        postImg.play.setOnClickListener(listenerVideo);
        postImg.play.setImageDrawable(AppCompatDrawableManager.get().getDrawable(activity, R.drawable.ic_play_circle));
        postImg.play.setVisibility(View.VISIBLE);

        if (i.getVideo().getTitle() != null && i.getVideo().getTitle().length() <= 20)
            postImg.title.setText(i.getVideo().getTitle());
        else if (i.getVideo().getTitle() != null)
            postImg.title.setText(i.getVideo().getTitle().substring(0, 20 - 1)
                    + activity.getResources().getString(R.string.more_text));

        setImg(i.getVideo().getPhoto_320(), postImg.img, width_l, height_l);
        postImg.img.setVisibility(View.VISIBLE);
        postImg.title.setVisibility(View.VISIBLE);
    }

    private void goToPhoto(List<String> list, int position) {
        new ImageViewer.Builder<>(activity, list)
                .setStartPosition(position)
                .show();
    }

    public boolean isAllView() {
        return isAllView;
    }

    public void setAllView(boolean allView) {
        isAllView = allView;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getTypeView();
    }

    public void addItem(CustomItemPost item) {
        long id = 0, from_id = 0;
        if (item.getTypeView() == Types.TYPE_WITH_AUTHOR_POST) {
            ModelPostWithAuthor model = (ModelPostWithAuthor) item;
            id = model.getId();
            from_id = model.getFrom_id();
        }
        if (!itemsSet.add(new Pair<>(id, from_id)))
            return;
        if (items.size() >= 0) {
            if (!isPB) {
                items.add(new ModelProgressBar());
                isPB = true;
            }

            items.add(getItemCount() - 1, item);
            notifyItemInserted(getItemCount() - 1);
        } else {
            items.add(item);
            notifyItemInserted(getItemCount() - 1);
        }
    }

    private CustomItemPost getItem(int position) {
        return items.get(position);
    }

    public int getSize() {
        return items.size() - (progressBar == null ? 0 : 1);
    }

    public void setEnd(boolean val) {
        isEnd = val;
        if (progressBar != null && val) {
            progressBar.setVisibility(View.GONE);
            notifyItemChanged(items.size() - 1);
        } else if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            notifyItemChanged(items.size() - 1);
        }
    }


    public void clearItems() {
        if (getItemCount() != 0) {
            itemsSet.clear();
            isPB = false;
            setEnd(false);
            items.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setColorScanned(boolean colorScanned) {
        this.isColorScanned = colorScanned;
    }

    private static class ItemProgressBarHolder extends RecyclerView.ViewHolder {
        ProgressBar modelPB;

        ItemProgressBarHolder(View itemView, ProgressBar modelPB) {
            super(itemView);
            this.modelPB = modelPB;
        }
    }

    private static class ItemPostHolder extends RecyclerView.ViewHolder {

        CardView postCV;
        ModelPostView mainPostView, historyPostView;
        AppCompatImageView openPost;
        TextView viewCounter, repostCounter, commentCounter, likeCounter;
        LinearLayout historyLL;

        ItemPostHolder(View itemView, CardView postCV, AppCompatImageView openPost,
                       TextView viewCounter, TextView repostCounter, TextView commentCounter,
                       TextView likeCounter, LinearLayout historyLL,
                       ModelPostView mainPostView, ModelPostView historyPostView) {
            super(itemView);
            this.postCV = postCV;
            this.mainPostView = mainPostView;
            this.historyPostView = historyPostView;
            this.openPost = openPost;
            this.viewCounter = viewCounter;
            this.repostCounter = repostCounter;
            this.commentCounter = commentCounter;
            this.likeCounter = likeCounter;
            this.historyLL = historyLL;
        }
    }

    private class ModelPostView {
        CircleImageView authorPhotoIV;
        TextView nameTV, dateTV, textTV, alarmTV, fixedTV, linkTV, pageTV;
        View postView;
        LinearLayout LL1, LL2, LL3;

        private int itAdd = 1;

        private ModelPostView(View postView) {
            this.postView = postView;
            createView(postView);
        }

        PostImg addPostImgs(int sizePost) {
            int lim = 4;
            if (sizePost <= 4)
                lim = 3;

            if (itAdd >= lim)
                itAdd = 1;

            PostImg p = null;
            switch (itAdd) {
                case 1:
                    p = new PostImg(LayoutInflater.from(LL1.getContext())
                            .inflate(R.layout.item_post, LL1, false));
                    LL1.addView(p.layout);
                    LL1.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    p = new PostImg(LayoutInflater.from(LL2.getContext())
                            .inflate(R.layout.item_post, LL2, false));
                    LL2.addView(p.layout);
                    LL2.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    p = new PostImg(LayoutInflater.from(LL3.getContext())
                            .inflate(R.layout.item_post, LL3, false));
                    LL3.addView(p.layout);
                    LL3.setVisibility(View.VISIBLE);
                    break;
            }

            itAdd++;
            return p;
        }

        private void createView(View v) {
            this.authorPhotoIV = (CircleImageView) v.findViewById(R.id.authorPhotoIV);
            this.nameTV = (TextView) v.findViewById(R.id.nameTV);
            this.dateTV = (TextView) v.findViewById(R.id.dateTV);
            this.textTV = (TextView) v.findViewById(R.id.textTV);
            this.linkTV = (TextView) v.findViewById(R.id.linkTV);
            this.pageTV = (TextView) v.findViewById(R.id.pageTV);
            this.alarmTV = (TextView) v.findViewById(R.id.alarmTV);
            this.fixedTV = (TextView) v.findViewById(R.id.fixedTV);
            textTV.setMovementMethod(LinkMovementMethod.getInstance());

            this.LL1 = (LinearLayout) v.findViewById(R.id.LL1);
            this.LL1.setVisibility(View.GONE);

            this.LL2 = (LinearLayout) v.findViewById(R.id.LL2);
            this.LL2.setVisibility(View.GONE);

            this.LL3 = (LinearLayout) v.findViewById(R.id.LL3);
            this.LL3.setVisibility(View.GONE);
        }

        void removeAllViews() {
            itAdd = 1;
            LL1.removeAllViews();
            this.LL1.setVisibility(View.GONE);

            LL2.removeAllViews();
            this.LL2.setVisibility(View.GONE);

            LL3.removeAllViews();
            this.LL3.setVisibility(View.GONE);
        }
    }

    private class PostImg {
        RelativeLayout layout;
        AppCompatImageView img, play;
        TextView title;

        PostImg(View v) {
            this.layout = (RelativeLayout) v.findViewById(R.id.layout);
            this.img = (AppCompatImageView) v.findViewById(R.id.postImg);

            this.title = (TextView) v.findViewById(R.id.titleTVpostImg);
            this.title.setVisibility(View.GONE);

            this.play = (AppCompatImageView) v.findViewById(R.id.postVideo);
            this.play.setVisibility(View.GONE);
        }
    }
}
