package ru.freshtape.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.freshtape.activities.ViewWallActivity;
import ru.freshtape.db.DBHelper;
import ru.freshtape.model.ModelGroupIntro;
import ru.freshtape.model.ModelProgressBar;
import ru.freshtape.model.ModelUserIntro;
import ru.freshtape.model.Types;
import ru.freshtape.R;
import ru.freshtape.model.CustomItemIntro;

/**
 * Created by Павел on 08.05.2017.
 */

public class ItemsIntroAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CustomItemIntro> items;
    private DBHelper dbHelper;
    private boolean isPB;
    private View progressBar;
    private Context context;
    private boolean isEnd;

    public ItemsIntroAdapter(Context context) {
        items = new ArrayList<>();
        isPB = false;
        isEnd = false;
        dbHelper = new DBHelper(context);
        this.context = context;
        progressBar = null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Types.TYPE_GROUP || viewType == Types.TYPE_USER) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.model_item_intro, parent, false);

            TextView nameTV = (TextView) v.findViewById(R.id.nameTV),
                    rialNameTV = (TextView) v.findViewById(R.id.typeTV);
            CircleImageView profilPhotoIV = (CircleImageView) v.findViewById(R.id.profilPhotoIV);
            Button addDelBtn = (Button) v.findViewById(R.id.addDelBtn);
            ProgressBar progresAddDelBtn = (ProgressBar) v.findViewById(R.id.progresAddDelBtn);

            return new ItemHolder(v, profilPhotoIV, nameTV, rialNameTV, addDelBtn, progresAddDelBtn);
        } else if (viewType == Types.TYPE_PROGERSS_BAR) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.model_progres_bar, parent, false);

            progressBar = v;

            ProgressBar modelPB = (ProgressBar) v.findViewById(R.id.modelPB);

            return new ItemProgressBarHolder(v, modelPB);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getTypeView();
    }

    public void addItem(CustomItemIntro item, boolean isNotify) {
        if (items.size() >= 11) {
            if (!isPB) {
                items.add(new ModelProgressBar());
                isPB = true;
            }

            items.add(getItemCount() - 1, item);
            if (isNotify)
                notifyItemInserted(getItemCount() - 1);
        } else {
            items.add(item);
            if (isNotify)
                notifyItemInserted(getItemCount() - 1);
        }
    }

    private CustomItemIntro getItem(int position) {
        return items.get(position);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final CustomItemIntro customItem = items.get(position);

        if (customItem.getTypeView() == Types.TYPE_GROUP || customItem.getTypeView() == Types.TYPE_USER) {
            final ItemHolder itemViewHolder = (ItemHolder) holder;
            holder.itemView.setEnabled(true);
            View itemView = itemViewHolder.itemView;

            itemViewHolder.addDelBtn.setText(context.getString(R.string.add));

            itemViewHolder.progresAddDelBtn.setVisibility(View.INVISIBLE);

            ModelGroupIntro modelGroupIntro = null;
            ModelUserIntro modelUserIntro = null;

            if (customItem.getTypeView() == Types.TYPE_GROUP) {
                modelGroupIntro = (ModelGroupIntro) customItem;
            } else if (customItem.getTypeView() == Types.TYPE_USER) {
                modelUserIntro = (ModelUserIntro) customItem;
            }

            final ModelGroupIntro finalModelGroupIntro = modelGroupIntro;
            final ModelUserIntro finalModelUserIntro = modelUserIntro;

            View.OnClickListener listenerGoView = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ViewWallActivity.class);
                    if (customItem.getTypeView() == Types.TYPE_GROUP) {
                        intent.putExtra(DBHelper.KEY_VK_ID, finalModelGroupIntro.getId());
                        intent.putExtra(DBHelper.KEY_NAME, finalModelGroupIntro.getName());
                        intent.putExtra(DBHelper.KEY_SHORT_NAME, finalModelGroupIntro.getScreen_name());
                        intent.putExtra(DBHelper.KEY_TYPE, finalModelGroupIntro.getType());
                        intent.putExtra(DBHelper.KEY_PHOTO_50, finalModelGroupIntro.getPhoto_50());
                        intent.putExtra(DBHelper.KEY_PHOTO_100, finalModelGroupIntro.getPhoto_100());
                        intent.putExtra(DBHelper.KEY_PHOTO_200, finalModelGroupIntro.getPhoto_200());
                        intent.putExtra(DBHelper.KEY_TYPE_INT, Types.TYPE_GROUP);
                    } else if (customItem.getTypeView() == Types.TYPE_USER) {
                        intent.putExtra(DBHelper.KEY_VK_ID, finalModelUserIntro.getId());
                        intent.putExtra(DBHelper.KEY_NAME, finalModelUserIntro.getFirst_name() + " " + finalModelUserIntro.getLast_name());
                        intent.putExtra(DBHelper.KEY_SHORT_NAME, finalModelUserIntro.getDomain());
                        intent.putExtra(DBHelper.KEY_TYPE, finalModelUserIntro.getType());
                        intent.putExtra(DBHelper.KEY_PHOTO_50, finalModelUserIntro.getPhoto_50());
                        intent.putExtra(DBHelper.KEY_PHOTO_100, finalModelUserIntro.getPhoto_100());
                        intent.putExtra(DBHelper.KEY_PHOTO_200, finalModelUserIntro.getPhoto_200());
                        intent.putExtra(DBHelper.KEY_TYPE_INT, Types.TYPE_USER);
                    }
                    context.startActivity(intent);
                }
            };

            itemViewHolder.profilPhotoIV.setOnClickListener(listenerGoView);
            itemViewHolder.itemView.setOnClickListener(listenerGoView);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemViewHolder.progresAddDelBtn.setVisibility(View.VISIBLE);
                    itemViewHolder.addDelBtn.setVisibility(View.INVISIBLE);

                    dbHelper.addItem(dbHelper, customItem,
                            new DBHelper.CallbackAddFavorites() {
                                @Override
                                public void del() {
                                    itemViewHolder.addDelBtn.setText(context.getString(R.string.add));
                                    itemViewHolder.addDelBtn.setBackground(context.getResources().getDrawable(R.drawable.ok_button_style));
                                }

                                @Override
                                public void add() {
                                    itemViewHolder.addDelBtn.setText(context.getString(R.string.del));
                                    itemViewHolder.addDelBtn.setBackground(context.getResources().getDrawable(R.drawable.cancel_button_style));
                                }
                            });

                    itemViewHolder.progresAddDelBtn.setVisibility(View.INVISIBLE);
                    itemViewHolder.addDelBtn.setVisibility(View.VISIBLE);
                }
            };

            itemViewHolder.addDelBtn.setOnClickListener(listener);

            long vk_id = -1;
            if (customItem.getTypeView() == Types.TYPE_GROUP) {
                vk_id = finalModelGroupIntro.getId();
            } else if (customItem.getTypeView() == Types.TYPE_USER) {
                vk_id = finalModelUserIntro.getId();
            }

            if (dbHelper.findItemInFavorites(dbHelper.getReadableDatabase(), vk_id)) {
                itemViewHolder.addDelBtn.setText(context.getString(R.string.del));
                itemViewHolder.addDelBtn.setBackground(context.getResources().getDrawable(R.drawable.cancel_button_style));
            } else {
                itemViewHolder.addDelBtn.setText(context.getString(R.string.add));
                itemViewHolder.addDelBtn.setBackground(context.getResources().getDrawable(R.drawable.ok_button_style));
            }

            if (customItem.getTypeView() == Types.TYPE_GROUP) {
                itemViewHolder.nameTV.setText(modelGroupIntro.getName());
                itemViewHolder.typeTV.setText(modelGroupIntro.getType());
                Picasso.with(itemView.getContext())
                        .load(modelGroupIntro.getPhoto_50())
                        .fit()
                        .placeholder(R.drawable.circle)
                        .error(R.drawable.circle)
                        .into(itemViewHolder.profilPhotoIV);

            } else if (customItem.getTypeView() == Types.TYPE_USER) {
                itemViewHolder.nameTV.setText(modelUserIntro.getFirst_name()
                        + " " + modelUserIntro.getLast_name());
                itemViewHolder.typeTV.setText(modelUserIntro.getDomain());


                Picasso.with(itemView.getContext())
                        .load(modelUserIntro.getPhoto_50())
                        .fit()
                        .placeholder(R.drawable.circle)
                        .error(R.drawable.circle)
                        .into(itemViewHolder.profilPhotoIV);

            }
        } else if (customItem.getTypeView() == Types.TYPE_PROGERSS_BAR) {
            ItemProgressBarHolder itemHolder = (ItemProgressBarHolder) holder;

            if (isEnd)
                itemHolder.modelPB.setVisibility(View.GONE);
            else {
                itemHolder.modelPB.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setEnd(boolean val) {
        isEnd = val;
        if (progressBar != null && val) {
            progressBar.setVisibility(View.GONE);
            notifyItemChanged(items.size() - 1);
        } else if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            notifyItemChanged(items.size() - 1);
        }
    }


    public void clearItems() {
        if (getItemCount() != 0) {
            isPB = false;
            setEnd(false);
            items.clear();
            items = new ArrayList<>();
            notifyDataSetChanged();
        }
    }

    public void removeItem(int location) {
        if (location >= 0 && location <= getItemCount() - 1) {
            items.remove(location);
            notifyItemRemoved(location);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        return true;
    }

    private static class ItemProgressBarHolder extends RecyclerView.ViewHolder {
        ProgressBar modelPB;

        ItemProgressBarHolder(View itemView, ProgressBar modelPB) {
            super(itemView);
            this.modelPB = modelPB;
        }
    }

    private static class ItemHolder extends RecyclerView.ViewHolder {

        TextView nameTV, typeTV;
        CircleImageView profilPhotoIV;
        Button addDelBtn;
        ProgressBar progresAddDelBtn;

        ItemHolder(View itemView, CircleImageView profilPhotoIV,
                   TextView nameTV, TextView typeTV, Button addDelBtn,
                   ProgressBar progresAddDelBtn) {
            super(itemView);
            this.typeTV = typeTV;
            this.nameTV = nameTV;
            this.profilPhotoIV = profilPhotoIV;
            this.addDelBtn = addDelBtn;
            this.progresAddDelBtn = progresAddDelBtn;
        }
    }
}
