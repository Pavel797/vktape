package ru.freshtape;

import android.content.Intent;
import android.support.v7.app.AppCompatDelegate;

import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import com.facebook.drawee.backends.pipeline.Fresco;

import ru.freshtape.activities.MainActivity;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.vk.VkApiHelper;

/**
 * Created by Павел on 03.05.2017.
 */

public class FreshTapeApplication extends android.app.Application {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_NO);
    }

    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                VkApiHelper.logout();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        PreferencesHelper.init(getApplicationContext());

        initializationYandexMetrica();

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                .setResizeAndRotateEnabledForNetwork(true)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this, config);

        initializationVKSdk();
    }

    private void initializationVKSdk() {
        vkAccessTokenTracker.startTracking();
        VKSdk.customInitialize(this, VkApiHelper.ID_APP, VkApiHelper.API_VERSION);
    }

    private void initializationYandexMetrica() {
        //Создание конструктора расширенной инициализации
        YandexMetricaConfig.Builder configBuilder
                = YandexMetricaConfig.newConfigBuilder(PreferencesHelper.YM_API_KEY);

        //Является ли запуск приложения первым.
        boolean res = !PreferencesHelper.isFirstApplicationLaunch();
        configBuilder.handleFirstActivationAsUpdate(res);

        //Создание объекта расширенной конфигурации
        YandexMetricaConfig extendedConfig = configBuilder.build();

        YandexMetrica.setSessionTimeout(1800);

        // Инициализация AppMetrica SDK
        YandexMetrica.activate(getApplicationContext(), extendedConfig);
        // Отслеживание активности пользователей
        YandexMetrica.enableActivityAutoTracking(this);
    }
}
