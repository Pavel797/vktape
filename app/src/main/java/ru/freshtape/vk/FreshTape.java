package ru.freshtape.vk;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.yandex.metrica.YandexMetrica;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import ru.freshtape.db.DBHelper;
import ru.freshtape.exception.ParsPostsException;
import ru.freshtape.model.CustomItemPost;
import ru.freshtape.model.ModelAnswerFirstPosts;
import ru.freshtape.model.ModelAnswerOnePost;
import ru.freshtape.model.ModelPostWithAuthor;
import ru.freshtape.model.SmallModelPost;

/**
 * Created by Павел on 09.07.2017.
 */

public class FreshTape {

    private DBHelper dbHelper;
    private TreeSet<SmallModelPost> items;
    private ViewedSet viewedSet;
    private long timeInit, timeGet;
    private Date date;

    private InitTask initTask;
    private GetPostTask getPostTask;

    private boolean isInit, isOffScannedPosts, isColorViewPost;

    public FreshTape(Context context) {
        dbHelper = new DBHelper(context);
        items = new TreeSet<>(new Comparator<SmallModelPost>() {
            public int compare(SmallModelPost a, SmallModelPost b) {
                if (a.getDate() == b.getDate())
                    return (int) (b.getFrom_id() - a.getFrom_id());
                return (int) (b.getDate() - a.getDate());
            }
        });
        viewedSet = new ViewedSet(dbHelper);
        date = new Date();

        timeInit = 0L;
        timeGet = 0L;
        isInit = false;
        isOffScannedPosts = false;
        isColorViewPost = false;
    }

    public void stop() {
        if (initTask != null && !initTask.isCancelled()) {
            initTask.cancel(false);
        }
        if (getPostTask != null && !getPostTask.isCancelled()) {
            getPostTask.cancel(false);
        }
        isInit = false;
        items.clear();
        viewedSet.clear();
    }

    public void init(CallbackInitTape callback) {
        initTask = new InitTask(callback);
        initTask.execute();
    }

    public void getPosts(Long count, CallbackGetTape callback) {
        getPostTask = new GetPostTask(callback);
        getPostTask.execute(count);
    }

    public void viewed(SmallModelPost post) {
        viewedSet.viewed(post);
    }

    public void saveViewed() {
        if (viewedSet.isLoad())
            viewedSet.saveViewed();
    }

    public boolean isOffScannedPosts() {
        return isOffScannedPosts;
    }

    public void setOffScannedPosts(boolean offScannedPosts) {
        isOffScannedPosts = offScannedPosts;
    }

    public boolean isColorViewPost() {
        return isColorViewPost;
    }

    public void setColorViewPost(boolean colorViewPost) {
        isColorViewPost = colorViewPost;
    }

    public synchronized boolean isInit() {
        return isInit;
    }

    private boolean isManyRequests(long time, long limitTime) {
        return (date.getTime() - time) < limitTime;
    }

    private class GetPostTask extends AsyncTask<Long, CustomItemPost, Boolean> {

        private CallbackGetTape callback;

        private GetPostTask(CallbackGetTape callback) {
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Long... params) {
            if (!isInit)
                throw new ExceptionInInitializerError("Tape must be init!");

            if (isCancelled() || params == null || params.length == 0)
                return null;

            for (int i = 0; i < params[0] && !items.isEmpty(); i++) {
                SmallModelPost sm = items.first();
                SmallModelPost maxPostDate = sm;
                if (isOffScannedPosts)
                    maxPostDate = viewedSet.getNext(sm);

                items.remove(sm);

                if (maxPostDate != null) {

                    if (isCancelled()) {
                        return null;
                    }

                    final Pair<ModelPostWithAuthor, SmallModelPost> p = getOnePost(maxPostDate);

                    if (p == null)
                        return null;

                    if (p.first == null) {
                        i--;
                        continue;
                    }

                /*if (maxPostDate.getId() != p.first.getId()) {
                    String str = "I can not get a post on id! id_put = " + maxPostDate.getId()
                            + " from_id = " + maxPostDate.getFrom_id() + " id_give = " + p.first.getId();
                    YandexMetrica.reportError("Problem with parsing posts", new ParsPostsException(str));
                }*/

                    if (isCancelled())
                        return null;

                    publishProgress(p.first);

                    if (isCancelled())
                        return null;

                    if (p.second != null && p.second.getDate() != 0)
                        items.add(p.second);
                } else {
                    if (isCancelled())
                        return null;

                    publishProgress();
                }
                if (items.size() == 0) {
                    publishProgress();
                    return null;
                }
            }
            return null;
        }

        private Pair<ModelPostWithAuthor, SmallModelPost> getOnePost(SmallModelPost post) {

            if (isManyRequests(timeGet, 800)) {
                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            timeGet = date.getTime();

            if (isCancelled())
                return null;

            final String[] jsonAnswer = {""};
            VkApiHelper.getPost(post.getFrom_id(), post.getId(), new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    jsonAnswer[0] = response.json.toString();
                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    super.attemptFailed(request, attemptNumber, totalAttempts);
                    callback.onError(request.toString());
                }

                @Override
                public void onError(VKError error) {
                    super.onError(error);
                    callback.onError(error.errorMessage);
                }
            });

            if (isCancelled())
                return null;

            if (jsonAnswer[0].isEmpty())
                return null;

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();

            ModelAnswerOnePost answer = gson.fromJson(jsonAnswer[0], ModelAnswerOnePost.class);

            if (answer == null)
                return null;

            if (isCancelled())
                return null;

            if (!isOffScannedPosts && isColorViewPost) {
                answer.getPost().setView(viewedSet.isViewed(new SmallModelPost(answer.getPost().getWall().getFrom_id(),
                        answer.getPost().getWall().getDate(),
                        answer.getPost().getWall().getId())));
            }
            return new Pair<>(answer.getPost(), answer.getSecond_post_date());
        }

        @Override
        protected void onProgressUpdate(CustomItemPost... values) {
            super.onProgressUpdate(values);
            if (values == null || values.length == 0) {
                callback.onEnd();
            } else {
                callback.onAdd(values[0]);
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            callback.onComplete();
        }
    }

    private class InitTask extends AsyncTask<Void, Void, Boolean> {

        private final int SIZE_INIT = 25;
        private CallbackInitTape callback;

        private InitTask(CallbackInitTape callback) {
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            items.clear();
            viewedSet.clear();

            if (isManyRequests(timeInit, 1200)) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            timeInit = date.getTime();

            // выгружаем данные о подписках из бд
            List<String> locItems = loadDataOfDB(dbHelper.getReadableDatabase());

            if (isCancelled())
                return false;

            boolean res = false;
            try {
                // инициализируем данные для выдачи запросами к api VK
                res = initItems(locItems);
            } catch (ParsPostsException e) {
                YandexMetrica.reportError("Problem with parsing posts", e);
            }

            // если всё збс, загружаем структурку для контроля за просмотренными постами
            if (!isCancelled() && res) {
                viewedSet.loadViewed();
            }

            return res;
        }

        private boolean initItems(List<String> locItems) throws ParsPostsException {
            for (String str : locItems) {
                final String[] jsonAnswer = {""};

                VkApiHelper.getTapeInit(str, new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        jsonAnswer[0] = response.json.toString();
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        super.attemptFailed(request, attemptNumber, totalAttempts);
                    }

                    @Override
                    public void onError(VKError error) {
                        super.onError(error);
                    }
                });

                if (isCancelled() || jsonAnswer[0].isEmpty())
                    return false;

                jsonAnswer[0] = jsonAnswer[0].replaceAll("false,", "");
                jsonAnswer[0] = jsonAnswer[0].replaceAll("false", "");

                if (isCancelled())
                    return false;

                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                ModelAnswerFirstPosts answer;
                try {
                    answer = gson.fromJson(jsonAnswer[0], ModelAnswerFirstPosts.class);
                } catch (Exception e) {
                    callback.onError();
                    throw new ParsPostsException("Problem with res = \"" + str + "\"");
                }

                if (isCancelled())
                    return false;

                for (SmallModelPost post : answer.getResponse()) {
                    if (post.getDate() != 0) {
                        if (isCancelled())
                            return false;
                        items.add(post);
                    }
                }
            }
            return true;
        }

        private List<String> loadDataOfDB(SQLiteDatabase db) {
            // вытягивание из бд инфу о группах
            Cursor cursor = db.rawQuery("select " + DBHelper.KEY_VK_ID + ", " + DBHelper.KEY_TYPE_INT
                    + " from " + DBHelper.TABLE_FAVORITES, new String[]{});

            List<String> locItems = new ArrayList<>();

            if (isCancelled())
                return null;

            if (cursor.moveToFirst()) {
                int id_vkIndex = cursor.getColumnIndex(DBHelper.KEY_VK_ID);
                int it = 1;
                String res = "-146724713,";
                do {
                    if (isCancelled())
                        return null;

                    if (it >= SIZE_INIT) {
                        if (res.charAt(res.length() - 1) == ',')
                            res = res.substring(0, res.length() - 1);

                        locItems.add(res);
                        res = "";
                        it = 0;
                    }
                    res += String.valueOf(cursor.getLong(id_vkIndex)) + ",";
                    it++;
                } while (cursor.moveToNext());
                if (!res.isEmpty()) {
                    if (res.charAt(res.length() - 1) == ',')
                        res = res.substring(0, res.length() - 1);
                    locItems.add(res);
                }
            }

            if (isCancelled())
                return null;

            cursor.close();
            db.close();
            dbHelper.close();

            return locItems;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            isInit = aBoolean;
            if (!aBoolean) {
                callback.onError();
            } else {
                callback.onComplete();
            }
        }
    }

    public interface CallbackInitTape {
        void onComplete();
        void onError();
    }

    public interface CallbackGetTape {
        void onComplete();
        void onAdd(CustomItemPost post);
        void onEnd();
        void onError(String error);
    }
}
