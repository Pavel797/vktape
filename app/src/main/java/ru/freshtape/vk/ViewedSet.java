package ru.freshtape.vk;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.util.LongSparseArray;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;
import java.util.TreeSet;

import ru.freshtape.db.DBHelper;
import ru.freshtape.model.SmallModelPost;

/**
 * Created by Павел on 05.06.2017.
 */

public class ViewedSet {

    // это рабочий
    private LongSparseArray<TreeSet<Pair<SmallModelPost, SmallModelPost>>> mapItemsGl;
    // это для сохранения
    private LongSparseArray<TreeSet<Pair<SmallModelPost, SmallModelPost>>> mapItemsViewedSaveGl;

    private DBHelper dbHelper;
    private boolean isLoad, isSaved;
    private int countSave;

    public ViewedSet(DBHelper dbHelper) {
        isLoad = false;
        mapItemsGl = new LongSparseArray<>();
        mapItemsViewedSaveGl = new LongSparseArray<>();
        this.dbHelper = dbHelper;
        countSave = 0;
    }

    public boolean isLoad() {
        return isLoad;
    }

    public void loadViewed() {
        if (dbHelper == null)
            throw new ExceptionInInitializerError("dbHelper is null");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + DBHelper.TABLE_VIEWED, new String[]{});

        if (cursor.moveToFirst()) {
            int vk_from_id = cursor.getColumnIndex(DBHelper.KEY_VK_FROM_ID);

            //int first = cursor.getColumnIndex(DBHelper.KEY_FIRST);
            int id_vk_first = cursor.getColumnIndex(DBHelper.KEY_VK_ID_FIRST);
            int date_first = cursor.getColumnIndex(DBHelper.KEY_DATE_FIRST);

            //int second = cursor.getColumnIndex(DBHelper.KEY_SECOND);
            int id_vk_second = cursor.getColumnIndex(DBHelper.KEY_VK_ID_SECOND);
            int date_second = cursor.getColumnIndex(DBHelper.KEY_DATE_SECOND);

            do {
                SmallModelPost first = new SmallModelPost(cursor.getLong(vk_from_id),
                                                            cursor.getLong(date_first),
                                                            cursor.getLong(id_vk_first));
                SmallModelPost second = new SmallModelPost(cursor.getLong(vk_from_id),
                                                            cursor.getLong(date_second),
                                                            cursor.getLong(id_vk_second));

                TreeSet<Pair<SmallModelPost, SmallModelPost>> items = mapItemsGl.get(first.getFrom_id());

                if (items == null) {
                    items = new TreeSet<>(new Comparator<Pair<SmallModelPost, SmallModelPost>>() {
                        public int compare(Pair<SmallModelPost, SmallModelPost> a, Pair<SmallModelPost, SmallModelPost> b) {
                            return (int) (b.first.getId() - a.first.getId());
                        }
                    });
                }

                items.add(new Pair<>(first, second));

                mapItemsGl.put(first.getFrom_id(), items);
                mapItemsViewedSaveGl.put(first.getFrom_id(), items);

            } while (cursor.moveToNext());
        }

        cursor.close();

        isLoad = true;
    }

    private void saveViewed(LongSparseArray<TreeSet<Pair<SmallModelPost, SmallModelPost>>> mapItems) {
        if (!isLoad)
            throw new ExceptionInInitializerError("ViewedSet must be load");

        if (dbHelper == null)
            throw new ExceptionInInitializerError("dbHelper is null");

        SQLiteDatabase writableDB = dbHelper.getWritableDatabase();

        for (int i = 0; mapItems != null && i < mapItems.size(); i++) {
            if (writableDB.isOpen()) {
                //try {
                    writableDB.delete(DBHelper.TABLE_VIEWED, DBHelper.KEY_VK_FROM_ID + "= ?", new String[]{String.valueOf(mapItems.keyAt(i))});
                //} catch (SQLiteException e) {
                //    Log.d("vktape", "Ничего");
                //}
            } else {
                break;
            }
            for (Pair<SmallModelPost, SmallModelPost> j: mapItems.valueAt(i)) {
                if (j == null)
                    continue;
                ContentValues cv = new ContentValues();

                cv.put(DBHelper.KEY_VK_FROM_ID, j.first.getFrom_id());

                cv.put(DBHelper.KEY_VK_ID_FIRST, j.first.getId());
                cv.put(DBHelper.KEY_DATE_FIRST, j.first.getDate());

                cv.put(DBHelper.KEY_VK_ID_SECOND, j.second.getId());
                cv.put(DBHelper.KEY_DATE_SECOND, j.second.getDate());
                if (writableDB.isOpen())
                    writableDB.insert(DBHelper.TABLE_VIEWED, null, cv);
            }
        }

        writableDB.close();
    }

    public boolean isViewed(SmallModelPost post) {
        if (!isLoad)
            throw new ExceptionInInitializerError("ViewedSet must be load");
        TreeSet<Pair<SmallModelPost, SmallModelPost>> items = mapItemsViewedSaveGl.get(post.getFrom_id());
        if (items == null)
            return false;
        Pair<SmallModelPost, SmallModelPost> it = items.floor(new Pair<>(post, post));
        // TODO: 11.07.2017 подумать над условием + протестировать
        return (it != null) && (it.second.getId() < post.getId() && post.getId() <= it.first.getId());
    }

    public void viewed(SmallModelPost post) {
        if (!isLoad)
            throw new ExceptionInInitializerError("ViewedSet must be load");
        insertInMapItemsViewedSaveGl(post);
        //if (countSave >= 4) {
        //    countSave = 0;
        //    saveViewed(mapItemsViewedSaveGl);
        //} else {
        //    countSave++;
        //}
    }

    public void saveViewed() {
        saveViewed(mapItemsViewedSaveGl);
    }

    public SmallModelPost getNext(SmallModelPost post) {
        if (!isLoad && dbHelper != null)
            throw new ExceptionInInitializerError("ViewedSet must be load");

        return insertInMapItemsGl(post);
    }

    public void clear() {
        mapItemsGl.clear();
        mapItemsViewedSaveGl.clear();
        if (dbHelper != null)
            dbHelper.close();
    }

    private void insertInMapItemsViewedSaveGl(SmallModelPost post) {
        TreeSet<Pair<SmallModelPost, SmallModelPost>> items = mapItemsViewedSaveGl.get(post.getFrom_id());
        if (items == null) {
            items = new TreeSet<>(new Comparator<Pair<SmallModelPost, SmallModelPost>>() {
                public int compare(Pair<SmallModelPost, SmallModelPost> a, Pair<SmallModelPost, SmallModelPost> b) {
                    return (int) (b.first.getId() - a.first.getId());
                }
            });
        }
        Iterator<Pair<SmallModelPost, SmallModelPost>> it = items.iterator();
        if (it.hasNext()) { // если мы имеем хотябы 1 промежуток, но может и 2
            Pair<SmallModelPost, SmallModelPost> a = it.next();
            if (it.hasNext()) { // если мы имеем 2 промежутка
                Pair<SmallModelPost, SmallModelPost> b = it.next();
                if (post.getId() > a.first.getId()) {
                    items.add(new Pair<>(post, post));

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                } else if (a.first.getId() >= post.getId() && post.getId() >= a.second.getId()) {

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                } else if (b.first.getId() >= post.getId() && post.getId() >= b.second.getId()) {
                    items.remove(a);
                    items.remove(b);
                    items.add(new Pair<>(a.first, b.second));

                    // схлопывание А и Б тут не надо, мы делаем это сами

                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                } else if (post.getId() < b.second.getId()) {
                    items.remove(b);
                    items.add(new Pair<>(b.first, post));
                    b = new Pair<>(b.first, post);

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                } else if (a.second.getId() >= post.getId() && post.getId() >= b.second.getId()) {
                    items.remove(a);
                    items.add(new Pair<>(a.first, post));
                    a = new Pair<>(a.first, post);

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                }
            } else { // если мы имеем 1 промежуток
                if (a.first.getId() >= post.getId() && post.getId() >= a.second.getId()) {
                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                } else if (post.getId() > a.first.getId()) {
                    items.add(new Pair<>(post, post));
                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                } else if (post.getId() < a.second.getId()) {
                    items.remove(a);
                    items.add(new Pair<>(a.first, post));
                    mapItemsViewedSaveGl.put(post.getFrom_id(), items);
                }
            }
        } else { // если мы не имеем промежутков
            items.add(new Pair<>(post, post));

            mapItemsViewedSaveGl.put(post.getFrom_id(), items);
        }
    }


    private SmallModelPost insertInMapItemsGl(SmallModelPost post) {
        TreeSet<Pair<SmallModelPost, SmallModelPost>> items = mapItemsGl.get(post.getFrom_id());
        if (items == null) {
            items = new TreeSet<>(new Comparator<Pair<SmallModelPost, SmallModelPost>>() {
                public int compare(Pair<SmallModelPost, SmallModelPost> a, Pair<SmallModelPost, SmallModelPost> b) {
                    return (int) (b.first.getId() - a.first.getId());
                }
            });
        }

        Iterator<Pair<SmallModelPost, SmallModelPost>> it = items.iterator();
        if (it.hasNext()) { // если мы имеем хотябы 1 промежуток, но может и 2
            Pair<SmallModelPost, SmallModelPost> a = it.next();
            if (it.hasNext()) { // если мы имеем 2 промежутка
                Pair<SmallModelPost, SmallModelPost> b = it.next();
                if (post.getId() > a.first.getId()) {
                    items.add(new Pair<>(post, post));

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsGl.put(post.getFrom_id(), items);
                    return post;
                } else if (a.first.getId() >= post.getId() && post.getId() >= a.second.getId()) {

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsGl.put(post.getFrom_id(), items);
                    return a.second;
                } else if (b.first.getId() >= post.getId() && post.getId() >= b.second.getId()) {
                    items.remove(a);
                    items.remove(b);
                    items.add(new Pair<>(a.first, b.second));

                    // схлопывание А и Б тут не надо, мы делаем это сами

                    mapItemsGl.put(post.getFrom_id(), items);
                    return b.second;
                } else if (post.getId() < b.second.getId()) {
                    items.remove(b);
                    items.add(new Pair<>(b.first, post));
                    b = new Pair<>(b.first, post);

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsGl.put(post.getFrom_id(), items);
                    return post;
                } else if (a.second.getId() >= post.getId() && post.getId() >= b.second.getId()) {
                    items.remove(a);
                    items.add(new Pair<>(a.first, post));
                    a = new Pair<>(a.first, post);

                    // схлопывание А и Б
                    if (a.second.getId() <= b.first.getId() || a.second.getId() == (b.first.getId() + 1)) {
                        items.remove(a);
                        items.remove(b);
                        items.add(new Pair<>(a.first, b.second));
                    }

                    mapItemsGl.put(post.getFrom_id(), items);
                    return post;
                }
            } else { // если мы имеем 1 промежуток
                if (a.first.getId() >= post.getId() && post.getId() >= a.second.getId()) {
                    mapItemsGl.put(post.getFrom_id(), items);
                    return a.second;
                } else if (post.getId() > a.first.getId()) {
                    items.add(new Pair<>(post, post));
                    mapItemsGl.put(post.getFrom_id(), items);
                    return post;
                } else if (post.getId() < a.second.getId()) {
                    items.remove(a);
                    items.add(new Pair<>(a.first, post));
                    mapItemsGl.put(post.getFrom_id(), items);
                    return post;
                }
            }
        } else { // если мы не имеем промежутков
            items.add(new Pair<>(post, post));

            mapItemsGl.put(post.getFrom_id(), items);
            return post;
        }

        return null;
    }

    private class Pair<F, S> {
        private final F first;
        private final S second;

        private Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Pair)) {
                return false;
            }
            Pair<?, ?> p = (Pair<?, ?>) o;
            return Objects.equals(p.first, first) && Objects.equals(p.second, second);
        }

        @Override
        public int hashCode() {
            return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode());
        }

        @Override
        public String toString() {
            return "Pair{" + String.valueOf(first) + " " + String.valueOf(second) + "}";
        }
    }

    /*public void setSaved(boolean saved) {
        this.isSaved = saved;
    }*/
}
