package ru.freshtape.vk;

import android.net.Uri;

import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.model.VKScopes;

import java.util.HashMap;
import java.util.Map;

import ru.freshtape.db.DBHelper;
import ru.freshtape.preferences.PreferencesHelper;

/**
 * Created by Павел on 07.05.2017.
 */

public class VkApiHelper {

    public static final long SLEEP_TIME = 1900;

    public static final int ID_APP = 6015079;
    public static final String API_VERSION = "5.65";

    public static final String[] scope = {VKScopes.GROUPS, VKScopes.FRIENDS, VKScopes.VIDEO, VKScopes.WALL};

    public static void searchItems(String url, VKRequest.VKRequestListener listener) {
        if (url == null)
            return;

        Uri uri = Uri.parse(url);
        String path = getPath(uri.getPath());

        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("url", path);
            parametersMap.put("fields", "domain,screen_name,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max");

        VKRequest request = new VKRequest("execute.searchItems", new VKParameters(parametersMap));
        request.executeWithListener(listener);
    }

    /*private static String getPath(String badPath) {
        String res = "";
        int countSl = 0;
        for (char i : badPath.toCharArray()) {
            if (countSl == 1)
                res += i;
            else if (countSl > 1)
                break;
            if (i == '/' || i == '\\')
                countSl++;
        }

        if (res.isEmpty()) {
            return badPath;
        } else {
            return res;
        }
    }*/

    private static String getPath(String url) {
        String res = Uri.parse(url).getPath();
        res = res.trim();
        res = res.replace("/", "");
        return res;
    }

    public static void searchGroup(String q, long offset, long count, VKRequest.VKRequestListener listener) {
        searchGroup(q, offset, count, null, null, null, null, null, listener);
    }

    public static void searchGroup(String q, Long offset, Long count, String type, Long country_id,
                                   Long city_id, Integer future, Integer sort, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("q", q);
        parametersMap.put("offset", offset);
        parametersMap.put("count", count);

        parametersMap.put("type", type);
        parametersMap.put("country_id", country_id);
        parametersMap.put("city_id", city_id);
        parametersMap.put("future", future);
        parametersMap.put("sort", sort);

        VKRequest request = VKApi.groups().search(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void searchUser(String q, long offset, long count, VKRequest.VKRequestListener listener) {
        searchUser(q, offset, count, null, null, null, null, null, null, listener);
    }

    public static void wallGetOnePost(Long from_id, Long id_post, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("posts", from_id + "_" + id_post);
        parametersMap.put("copy_history_depth", 1);
        parametersMap.put("extended", 1);
        parametersMap.put("fields", "domain,screen_name,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max");


        VKRequest request = VKApi.wall().getById(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    private static void searchUser(String q, Long offset, Long count,
                                   Long city, Long country, Integer sex, Integer status,
                                   Integer online, Integer has_photo,
                                   VKRequest.VKRequestListener listener) {

        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("q", q);
        parametersMap.put("offset", offset);
        parametersMap.put("count", count);
        parametersMap.put("fields", "domain,screen_name,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max");

        parametersMap.put("city", city);
        parametersMap.put("country", country);
        parametersMap.put("sex", sex);
        parametersMap.put("status", status);
        parametersMap.put("online", online);
        parametersMap.put("has_photo", has_photo);

        VKRequest request = VKApi.users().search(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void getTapeInit(String items, VKRequest.VKRequestListener listener) {
        if (items == null || items.isEmpty())
            return;

        items = items.replaceAll("\\s+", " ");

        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("items", items);

        VKRequest request = new VKRequest("execute.getTapeInit", new VKParameters(parametersMap)); //VKApi.wall().get(new VKParameters(parametersMap));
        request.executeSyncWithListener(listener);
    }

    public static void getPost(Long owner_id, Long id, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("from_id", String.valueOf(owner_id));
        parametersMap.put("end_id", id);

        VKRequest request = new VKRequest("execute.getPost", new VKParameters(parametersMap));

        request.executeSyncWithListener(listener);
    }

    // пока не работает! А нужен ли он?
    public static void getNextPost(Long owner_id, Long id, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("from_id", String.valueOf(owner_id));
        parametersMap.put("end_id", id);

        VKRequest request = new VKRequest("execute.getNextPost", new VKParameters(parametersMap));

        request.executeSyncWithListener(listener);
    }

    public static void logout() {
        VKSdk.logout();
        PreferencesHelper.setUserId(-1);
        DBHelper db = new DBHelper(PreferencesHelper.context);
        db.delete(db.getWritableDatabase());
    }

    public static void getVideo(String videos, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("videos", videos);

        VKRequest request = VKApi.video().get(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void wallGet(Long owner_id, Long count, Long offset, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("offset", offset);
        parametersMap.put("count", count);
        parametersMap.put("owner_id", owner_id);
        parametersMap.put("extended", 1);
        parametersMap.put("filter", "owner");
        parametersMap.put("fields", "domain,screen_name,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max");

        VKRequest request = VKApi.wall().get(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void getMyFriends(Long count, Long offset, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("offset", offset);
        parametersMap.put("count", count);
        parametersMap.put("hints", "hints");
        parametersMap.put("fields", "domain,screen_name,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max");

        VKRequest request = VKApi.friends().get(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    public static void getMyGroups(Long count, Long offset, VKRequest.VKRequestListener listener) {
        Map<String, Object> parametersMap = new HashMap<>();

        parametersMap.put("offset", offset);
        parametersMap.put("count", count);
        parametersMap.put("extended", 1);
        //parametersMap.put("fields", "domain,screen_name,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max");

        VKRequest request = VKApi.groups().get(new VKParameters(parametersMap));

        request.executeWithListener(listener);
    }

    /*public static void test() {

        for (int i = 0; i < 300; i += 100) {
            final ModelAnswerPost[] answer = {null};

            getWall(100, i, new VKRequest.VKRequestListener() {

                @Override
                public void onError(VKError error) {
                    super.onError(error);
                }

                @Override
                public void onComplete(VKResponse response) {

                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    answer[0] = gson.fromJson(response.json.toString(), ModelAnswerPost.class);
                }
            });

            for (ModelPost j : answer[0].getResponse().getItems()) {
                Map<String, Object> parametersMap = new HashMap<>();

                parametersMap.put("object", "wall289297168_" + j.getId());
                parametersMap.put("group_id", "148581827");

                VKRequest request = VKApi.wall().repost(new VKParameters(parametersMap));

                request.executeSyncWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        super.attemptFailed(request, attemptNumber, totalAttempts);
                    }

                    @Override
                    public void onError(VKError error) {
                        super.onError(error);
                    }

                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);
                    }
                });

                try {
                    Thread.sleep(1800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }*/
}
