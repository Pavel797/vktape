package ru.freshtape.fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.Toast;

import ru.freshtape.model.ModelGroupIntro;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.R;
import ru.freshtape.adapters.ItemsIntroAdapter;
import ru.freshtape.db.DBHelper;
import ru.freshtape.model.CustomItemIntro;
import ru.freshtape.model.ModelUserIntro;
import ru.freshtape.model.Types;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends Fragment {

    private ProgressBar findPB;
    private SearchView findSV;
    private LinearLayoutManager layoutManager;
    private ItemsIntroAdapter adapter;
    private RadioButton radioGroupBtn, radioUserBtn, radioAllBtn;
    private SwipeRefreshLayout refreshFavoritesSRV;
    private RecyclerView rvFindFragment;
    //private AppCompatImageView onFavoritesPosts;

    private SwipeRefreshLayout.OnRefreshListener refreshListener;

    private DBHelper dbHelper;

    private static int COUNT_FIND = 20;
    boolean userScrolled, isLoad;
    private int size;

    public FavoritesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);

        size = 0;
        userScrolled = false;
        isLoad = false;

        findPB = (ProgressBar) rootView.findViewById(R.id.findPB);
        findPB.setVisibility(View.INVISIBLE);

        findSV = (SearchView) rootView.findViewById(R.id.findFavoritesSV);
        findSV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    findPB.setVisibility(View.INVISIBLE);
                } else if (newText.length() >= PreferencesHelper.MIN_LENGTH_FIND) {
                    findPB.setVisibility(View.VISIBLE);
                }
                updateAllItems();
                return false;
            }
        });

        //onFavoritesPosts = (AppCompatImageView) rootView.findViewById(R.id.onFavoritesPosts);

        radioGroupBtn = (RadioButton) rootView.findViewById(R.id.radioGroupBtn);
        radioUserBtn = (RadioButton) rootView.findViewById(R.id.radioUserBtn);
        radioAllBtn = (RadioButton) rootView.findViewById(R.id.radioAllBtn);

        refreshFavoritesSRV = (SwipeRefreshLayout) rootView.findViewById(R.id.refreshFavoritesSRV);

        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateAllItems();
                refreshFavoritesSRV.setRefreshing(false);
            }
        };

        refreshFavoritesSRV.setOnRefreshListener(refreshListener);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.radioUserBtn:
                        updateAllItems();
                    case R.id.radioAllBtn:
                        updateAllItems();
                        break;
                    case R.id.radioGroupBtn:
                        updateAllItems();
                        break;
                    /*case R.id.onFavoritesPosts:
                        Toast.makeText(getActivity(), "Пока не работат :(", Toast.LENGTH_SHORT).show();
                        // TODO: 05.06.2017
                        break;*/
                }
            }
        };

        //onFavoritesPosts.setOnClickListener(listener);
        radioGroupBtn.setOnClickListener(listener);
        radioUserBtn.setOnClickListener(listener);
        radioAllBtn.setOnClickListener(listener);

        rvFindFragment = (RecyclerView) rootView.findViewById(R.id.rvFavoritesFragment);
        layoutManager = new LinearLayoutManager(getActivity());

        MyScrollListener scrollListener = new MyScrollListener();

        rvFindFragment.addOnScrollListener(scrollListener);
        rvFindFragment.setLayoutManager(layoutManager);

        adapter = new ItemsIntroAdapter(getActivity());

        // установка адаптера
        rvFindFragment.setAdapter(adapter);

        dbHelper = new DBHelper(getActivity());

        refreshListener.onRefresh();

        return rootView;
    }

    @Override
    public void onStart() {
        updateAllItems();
        super.onStart();

        View  thisView = getView();
        if (thisView != null)
            thisView.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fadein));
    }

    @Override
    public void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        Context context = recyclerView.getContext();
        LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    private void searchUser(final String q, final int offset, final int count) {
        if (isLoad)
            return;
        isLoad = true;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = null;

        if (q == null || q.isEmpty()) {
            cursor = db.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                            DBHelper.KEY_TYPE_INT + " = ?" + " limit " + count + " offset " + offset,
                    new String[]{String.valueOf(Types.TYPE_USER)});
        } else {
            cursor = db.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                            DBHelper.KEY_NAME + " like ? " + " and "  + DBHelper.KEY_TYPE_INT + " = ?"
                            + " limit " + count + " offset " + offset,
                    new String[]{"%" + q + "%", String.valueOf(Types.TYPE_USER)});
        }

        if (cursor.moveToFirst()) {
            int id_vkIndex = cursor.getColumnIndex(DBHelper.KEY_VK_ID);
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
            int typeIndex = cursor.getColumnIndex(DBHelper.KEY_TYPE);
            int typeIntIndex = cursor.getColumnIndex(DBHelper.KEY_TYPE_INT);
            int short_nameIndex = cursor.getColumnIndex(DBHelper.KEY_SHORT_NAME);
            int photo_50Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_50);
            int photo_100Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_100);
            int photo_200Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_200);

            boolean isFirst = false;
            if (size == 0)
                isFirst = true;

            do {
                switch (cursor.getInt(typeIntIndex)) {
                    case Types.TYPE_USER:
                        ModelUserIntro modelUser = new ModelUserIntro();
                        modelUser.setId(cursor.getInt(id_vkIndex));
                        modelUser.setFirst_name(cursor.getString(nameIndex));
                        modelUser.setLast_name("");
                        modelUser.setType(cursor.getString(typeIndex));
                        modelUser.setDomain(cursor.getString(short_nameIndex));
                        modelUser.setPhoto_50(cursor.getString(photo_50Index));
                        modelUser.setPhoto_100(cursor.getString(photo_100Index));
                        modelUser.setPhoto_200(cursor.getString(photo_200Index));

                        addItems(modelUser, !isFirst);
                        break;
                    case Types.TYPE_GROUP:
                        ModelUserIntro modelGroup = new ModelUserIntro();
                        modelGroup.setId(cursor.getInt(id_vkIndex));
                        modelGroup.setFirst_name(cursor.getString(nameIndex));
                        modelGroup.setLast_name("");
                        modelGroup.setType(cursor.getString(typeIndex));
                        modelGroup.setDomain(cursor.getString(short_nameIndex));
                        modelGroup.setPhoto_50(cursor.getString(photo_50Index));
                        modelGroup.setPhoto_100(cursor.getString(photo_100Index));
                        modelGroup.setPhoto_200(cursor.getString(photo_200Index));

                        addItems(modelGroup, !isFirst);
                        break;

                }
            } while (cursor.moveToNext());

            if (isFirst)
                runLayoutAnimation(rvFindFragment);
        }

        size += cursor.getCount();

        if (cursor.getCount() == 0)
            adapter.setEnd(true);

        cursor.close();
        db.close();
        dbHelper.close();

        isLoad = false;
    }

    private void searchGroup(String q, int offset, final int count) {
        if (isLoad)
            return;
        isLoad = true;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor;

        if (q == null || q.isEmpty()) {
            cursor = db.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                    DBHelper.KEY_TYPE_INT + " = ?" + " limit " + count + " offset " + offset,
                    new String[]{String.valueOf(Types.TYPE_GROUP)});
        } else {
            cursor = db.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                    DBHelper.KEY_NAME + " like ? " + " and "  + DBHelper.KEY_TYPE_INT + " = ?"
                    + " limit " + count + " offset " + offset,
                    new String[]{"%" + q + "%", String.valueOf(Types.TYPE_GROUP)});
        }

        if (cursor == null) {
            Toast.makeText(getActivity().getApplicationContext(), R.string.warning, Toast.LENGTH_SHORT).show();
            return;
        }

        if (cursor.moveToFirst()) {
            int id_vkIndex = cursor.getColumnIndex(DBHelper.KEY_VK_ID);
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
            int typeIndex = cursor.getColumnIndex(DBHelper.KEY_TYPE);
            int typeIntIndex = cursor.getColumnIndex(DBHelper.KEY_TYPE_INT);
            int short_nameIndex = cursor.getColumnIndex(DBHelper.KEY_SHORT_NAME);
            int photo_50Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_50);
            int photo_100Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_100);
            int photo_200Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_200);

            boolean isFirst = false;
            if (size == 0)
                isFirst = true;

            do {
                switch (cursor.getInt(typeIntIndex)) {
                    case Types.TYPE_USER:
                        ModelUserIntro modelUser = new ModelUserIntro();
                        modelUser.setId(cursor.getInt(id_vkIndex));
                        modelUser.setFirst_name(cursor.getString(nameIndex));
                        modelUser.setLast_name("");
                        modelUser.setType(cursor.getString(typeIndex));
                        modelUser.setDomain(cursor.getString(short_nameIndex));
                        modelUser.setPhoto_50(cursor.getString(photo_50Index));
                        modelUser.setPhoto_100(cursor.getString(photo_100Index));
                        modelUser.setPhoto_200(cursor.getString(photo_200Index));

                        addItems(modelUser, !isFirst);
                        break;
                    case Types.TYPE_GROUP:
                        ModelUserIntro modelGroup = new ModelUserIntro();
                        modelGroup.setId(cursor.getInt(id_vkIndex));
                        modelGroup.setFirst_name(cursor.getString(nameIndex));
                        modelGroup.setLast_name("");
                        modelGroup.setType(cursor.getString(typeIndex));
                        modelGroup.setDomain(cursor.getString(short_nameIndex));
                        modelGroup.setPhoto_50(cursor.getString(photo_50Index));
                        modelGroup.setPhoto_100(cursor.getString(photo_100Index));
                        modelGroup.setPhoto_200(cursor.getString(photo_200Index));

                        addItems(modelGroup, !isFirst);
                        break;

                }
            } while (cursor.moveToNext());

            if (isFirst)
                runLayoutAnimation(rvFindFragment);
        }

        size += cursor.getCount();

        if (cursor.getCount() == 0)
            adapter.setEnd(true);

        cursor.close();
        db.close();
        dbHelper.close();

        isLoad = false;
    }

    private void searchAll(String q, int offset, int count) {
        if (isLoad)
            return;
        isLoad = true;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor;

        if (q == null || q.isEmpty()) {
            cursor = db.rawQuery("select * from " + DBHelper.TABLE_FAVORITES
                    + " limit " + count + " offset " + offset, new String[]{});
        } else {
            cursor = db.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                    DBHelper.KEY_NAME + " like ? "
                    + " limit " + count + " offset " + offset, new String[]{"%" + q + "%"});
        }

        if (cursor.moveToFirst()) {
            int id_vkIndex = cursor.getColumnIndex(DBHelper.KEY_VK_ID);
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
            int typeIndex = cursor.getColumnIndex(DBHelper.KEY_TYPE);
            int typeIntIndex = cursor.getColumnIndex(DBHelper.KEY_TYPE_INT);
            int short_nameIndex = cursor.getColumnIndex(DBHelper.KEY_SHORT_NAME);
            int photo_50Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_50);
            int photo_100Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_100);
            int photo_200Index = cursor.getColumnIndex(DBHelper.KEY_PHOTO_200);

            boolean isFirst = false;
            if (size == 0)
                isFirst = true;

            do {
                switch (cursor.getInt(typeIntIndex)) {
                    case Types.TYPE_USER:
                        ModelUserIntro modelUser = new ModelUserIntro();
                        modelUser.setId(cursor.getInt(id_vkIndex));
                        modelUser.setFirst_name(cursor.getString(nameIndex));
                        modelUser.setLast_name("");
                        modelUser.setType(cursor.getString(typeIndex));
                        modelUser.setDomain(cursor.getString(short_nameIndex));
                        modelUser.setPhoto_50(cursor.getString(photo_50Index));
                        modelUser.setPhoto_100(cursor.getString(photo_100Index));
                        modelUser.setPhoto_200(cursor.getString(photo_200Index));

                        addItems(modelUser, !isFirst);
                        break;
                    case Types.TYPE_GROUP:
                        ModelGroupIntro modelGroup = new ModelGroupIntro();
                        modelGroup.setId(cursor.getInt(id_vkIndex));
                        modelGroup.setName(cursor.getString(nameIndex));
                        modelGroup.setType(cursor.getString(typeIndex));
                        modelGroup.setScreen_name(cursor.getString(short_nameIndex));
                        modelGroup.setPhoto_50(cursor.getString(photo_50Index));
                        modelGroup.setPhoto_100(cursor.getString(photo_100Index));
                        modelGroup.setPhoto_200(cursor.getString(photo_200Index));

                        addItems(modelGroup, !isFirst);
                        break;

                }
            } while (cursor.moveToNext());

            if (isFirst)
                runLayoutAnimation(rvFindFragment);
        }

        size += cursor.getCount();

        if (cursor.getCount() == 0)
            adapter.setEnd(true);

        cursor.close();
        db.close();
        dbHelper.close();

        isLoad = false;
    }

    public void addSearch() {
        if (radioUserBtn.isChecked()) {
            searchUser(findSV.getQuery().toString(), size, COUNT_FIND);
        } else if (radioGroupBtn.isChecked()) {
            searchGroup(findSV.getQuery().toString(), size, COUNT_FIND);
        } else if (radioAllBtn.isChecked()) {
            searchAll(findSV.getQuery().toString(), size, COUNT_FIND);
        }
    }

    private void addItems(CustomItemIntro i, boolean isNotify) {
        adapter.addItem(i, isNotify);
    }

    private void updateAllItems() {
        size = 0;
        adapter.clearItems();

        if (radioUserBtn.isChecked()) {
            searchUser(findSV.getQuery().toString(), 0, COUNT_FIND);
        } else if (radioGroupBtn.isChecked()) {
            searchGroup(findSV.getQuery().toString(), 0, COUNT_FIND);
        } else if (radioAllBtn.isChecked()) {
            searchAll(findSV.getQuery().toString(), 0, COUNT_FIND);
        }
    }

    public void clear() {
        if (adapter != null)
            adapter.clearItems();
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount && dy > 0) {
                userScrolled = false;
                addSearch();
            }
        }
    }
}
