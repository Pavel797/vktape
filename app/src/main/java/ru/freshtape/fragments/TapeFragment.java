package ru.freshtape.fragments;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.yandex.metrica.YandexMetrica;

import ru.freshtape.activities.MainActivity;
import ru.freshtape.model.CustomItemPost;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.vk.FreshTape;
import ru.freshtape.R;
import ru.freshtape.adapters.PostAdapter;
import ru.freshtape.exception.ParsPostsException;

public class TapeFragment extends Fragment {

    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout refreshTapeSRL;
    private SwipeRefreshLayout.OnRefreshListener refreshListener;

    private LinearLayout goToFindFragment;

    private static int COUNT_GIVE = 6;
    boolean userScrolled, isLoad, isGet;
    private RecyclerView rvTapeFragment;
    private MyScrollListener scrollListener;

    private FreshTape freshTape;

    private PostAdapter adapter;

    public TapeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tape, container, false);

        userScrolled = false;
        isLoad = false;

        refreshTapeSRL = (SwipeRefreshLayout) rootView.findViewById(R.id.refreshTapeSRL);

        refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                freshTape.saveViewed();
                freshTape.stop();
                freshTape.setOffScannedPosts(PreferencesHelper.isOffScannedPosts());
                freshTape.setColorViewPost(PreferencesHelper.isColorScannedPosts());
                adapter.setColorScanned(freshTape.isColorViewPost()); // frashTape.isOffScannedPosts() ||
                adapter.setEnd(false);
                isLoad = false;
                stop();
                ((MainActivity) getActivity()).setGoToUpVisibility(View.INVISIBLE);
                goToFindFragment.setVisibility(View.INVISIBLE);
                refreshTapeSRL.setRefreshing(true);
                clearAllItems();
                ((MainActivity) getActivity()).showViews();
                ((MainActivity) getActivity()).setGoToUpVisibility(View.INVISIBLE);
                refreshTape();
            }
        };

        refreshTapeSRL.setProgressViewOffset(false, refreshTapeSRL.getProgressViewStartOffset(),
                refreshTapeSRL.getProgressViewEndOffset());
        refreshTapeSRL.setOnRefreshListener(refreshListener);

        rvTapeFragment = (RecyclerView) rootView.findViewById(R.id.rvTapeFragment);
        layoutManager = new LinearLayoutManager(getActivity());

        scrollListener = new MyScrollListener();

        rvTapeFragment.addOnScrollListener(scrollListener);
        rvTapeFragment.setLayoutManager(layoutManager);

        adapter = new PostAdapter(getActivity());

        // установка адаптера
        rvTapeFragment.setAdapter(adapter);

        goToFindFragment = (LinearLayout) rootView.findViewById(R.id.goToFindFragment);
        goToFindFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).onFindFragment();
            }
        });

        freshTape = new FreshTape(getActivity());
        adapter.setTapeHelper(freshTape);

        refreshListener.onRefresh();

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        View  thisView = getView();
        if (thisView != null)
            thisView.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fadein));
    }

    private void stop() {
        if (freshTape != null)
            freshTape.stop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        freshTape.saveViewed();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        freshTape.saveViewed();
        freshTape.stop();
        stop();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        freshTape.saveViewed();
        freshTape.stop();
        stop();
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        if (freshTape != null)
            freshTape.stop();
        stop();
        super.onAttach(context);
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        if (freshTape != null)
            freshTape.stop();
        super.onAttachFragment(childFragment);
    }

    public void addItems(CustomItemPost i) {
        if (refreshTapeSRL.isRefreshing())
            refreshTapeSRL.setRefreshing(false);
        adapter.addItem(i);
    }

    public void getMorePost() {
        getPost(COUNT_GIVE);
    }

    private void clearAllItems() {
        freshTape.stop();
        adapter.clearItems();
    }

    private void refreshTape() {
        if (isLoad)
            return;

        isLoad = true;
        freshTape.init(new FreshTape.CallbackInitTape() {
            @Override
            public void onComplete() {
                isLoad = false;
                getPost(COUNT_GIVE * 2);
            }

            @Override
            public void onError() {
                isLoad = false;
                Toast.makeText(getActivity(), R.string.failed_init_tape, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getPost(final long count) {
        if (!freshTape.isInit() || isLoad)
            return;


        isLoad = true;
        goToFindFragment.setVisibility(View.INVISIBLE);
        isGet = false;


        freshTape.getPosts(count, new FreshTape.CallbackGetTape() {
            @Override
            public void onComplete() {
                if (adapter.getSize() == 0)
                    goToFindFragment.setVisibility(View.VISIBLE);
                refreshTapeSRL.setRefreshing(false);
                isLoad = false;
                if (isGet)
                    getMorePost();
            }

            @Override
            public void onAdd(CustomItemPost post) {
                refreshTapeSRL.setRefreshing(false);
                addItems(post);
            }

            @Override
            public void onEnd() {
                isLoad = false;
                refreshTapeSRL.setRefreshing(false);
                setEnd();
            }

            @Override
            public void onError(String error) {
                isLoad = false;
                refreshTapeSRL.setRefreshing(false);
                String str = "onError in tape fragment error: " + error;
                YandexMetrica.reportError("Problem with parsing posts", new ParsPostsException(str));
            }
        });
    }

    public void scrollUP(int force) {
        if (rvTapeFragment == null)
            return;
        rvTapeFragment.smoothScrollBy(0, force);
        userScrolled = true;
    }

    public void scrollDOWN(int force) {
        if (rvTapeFragment == null)
            return;
        rvTapeFragment.smoothScrollBy(0, force);
        userScrolled = true;
    }

    public void goToUp() {
        rvTapeFragment.scrollToPosition(0);
    }

    public void setEnd() {
        adapter.setEnd(true);
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private static final int HIDE_THRESHOLD = 100;
        private int scrolledDistance = 0;
        private boolean controlsVisible = true;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                isGet = true;
                getMorePost();
            }

            // видимость/невидемость
            if (pastVisiblesItems == 0) {
                if (!controlsVisible) {
                    ((MainActivity) getActivity()).showViews();
                    ((MainActivity) getActivity()).setGoToUpVisibility(View.VISIBLE);
                    controlsVisible = true;
                }
            } else {
                if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                    ((MainActivity) getActivity()).hideViews();
                    controlsVisible = false;
                    scrolledDistance = 0;
                } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
                    ((MainActivity) getActivity()).showViews();
                    ((MainActivity) getActivity()).setGoToUpVisibility(View.VISIBLE);
                    controlsVisible = true;
                    scrolledDistance = 0;
                }
            }

            if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
                scrolledDistance += dy;
            }
        }
    }
}
