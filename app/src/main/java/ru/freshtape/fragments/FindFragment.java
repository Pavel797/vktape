package ru.freshtape.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.Date;

import ru.freshtape.R;
import ru.freshtape.activities.ViewFriendsGroupActivity;
import ru.freshtape.adapters.ItemsIntroAdapter;
import ru.freshtape.dialog.DialogAddInFavorit;
import ru.freshtape.model.CustomItemIntro;
import ru.freshtape.model.ModelAnswerGroup;
import ru.freshtape.model.ModelAnswerUser;
import ru.freshtape.model.ModelGroupIntro;
import ru.freshtape.model.ModelUserIntro;
import ru.freshtape.model.Types;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.vk.VkApiHelper;
import ru.freshtape.vk.VkErrorHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindFragment extends Fragment {

    private ProgressBar findPB;
    private SearchView findSV;
    private LinearLayoutManager layoutManager;
    private ItemsIntroAdapter adapter;
    private RadioButton radioGroupBtn, radioUserBtn;
    private RecyclerView rvFindFragment;
    private Button searchOnLinkBtn, searchOnGroups, searchOnFriends; // filterFindBtn,
    private LinearLayout layoutWithBtn;

    private static int COUNT_FIND = 20;
    boolean userScrolled, isLoad;
    private int size;

    private long backTime;

    public FindFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find, container, false);

        backTime = 0;
        size = 0;
        userScrolled = false;
        isLoad = false;

        layoutWithBtn = (LinearLayout) rootView.findViewById(R.id.layoutWithBtn);

        findPB = (ProgressBar) rootView.findViewById(R.id.findPB);
        findPB.setVisibility(View.INVISIBLE);

        findSV = (SearchView) rootView.findViewById(R.id.findSV);
        findSV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() >= PreferencesHelper.MIN_LENGTH_FIND)
                    updateAllItems();
                else {
                    clearAllItems();
                }
                return false;
            }
        });

        radioGroupBtn = (RadioButton) rootView.findViewById(R.id.radioGroupBtn);
        radioUserBtn = (RadioButton) rootView.findViewById(R.id.radioUserBtn);
        searchOnLinkBtn = (Button) rootView.findViewById(R.id.searchOnLinkBtn);
        //filterFindBtn = (Button) rootView.findViewById(R.id.filerFindBtn);
        searchOnFriends = (Button) rootView.findViewById(R.id.searchOnFriends);
        searchOnGroups = (Button) rootView.findViewById(R.id.searchOnGroups);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    /*case R.id.filerFindBtn:
                        Toast.makeText(getActivity(), "Пока не работат :(", Toast.LENGTH_SHORT).show();
                        break;*/
                    case R.id.radioUserBtn:
                        updateAllItems();
                        break;
                    case R.id.radioGroupBtn:
                        updateAllItems();
                        break;
                    case R.id.searchOnLinkBtn:
                        searchOnReference();
                        break;
                    case R.id.searchOnFriends:
                        Intent intent1 = new Intent(getActivity(), ViewFriendsGroupActivity.class);
                        intent1.putExtra(PreferencesHelper.TYPE, Types.TYPE_USER);
                        getActivity().startActivity(intent1);
                        break;
                    case R.id.searchOnGroups:
                        Intent intent2 = new Intent(getActivity(), ViewFriendsGroupActivity.class);
                        intent2.putExtra(PreferencesHelper.TYPE, Types.TYPE_GROUP);
                        getActivity().startActivity(intent2);
                        break;
                }
            }
        };

        searchOnFriends.setOnClickListener(listener);
        searchOnGroups.setOnClickListener(listener);
        radioGroupBtn.setOnClickListener(listener);
        radioUserBtn.setOnClickListener(listener);
        searchOnLinkBtn.setOnClickListener(listener);
        //filterFindBtn.setOnClickListener(listener);

        rvFindFragment = (RecyclerView) rootView.findViewById(R.id.rvFindFragment);
        layoutManager = new LinearLayoutManager(getActivity());

        MyScrollListener scrollListener = new MyScrollListener();

        rvFindFragment.addOnScrollListener(scrollListener);
        rvFindFragment.setLayoutManager(layoutManager);

        adapter = new ItemsIntroAdapter(getActivity());

        // установка адаптера
        rvFindFragment.setAdapter(adapter);

        return rootView;
    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        Context context = recyclerView.getContext();
        LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    private void clearAllItems() {
        size = 0;
        if (adapter != null)
            adapter.clearItems();
        if (findPB != null)
            findPB.setVisibility(View.INVISIBLE);
        if (layoutWithBtn != null)
            layoutWithBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        updateAllItems();
        super.onStart();

        View  thisView = getView();
        if (thisView != null)
            thisView.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fadein));
    }

    private void searchOnReference() {
        DialogFragment dialogAddInFavorit = new DialogAddInFavorit();
        dialogAddInFavorit.show(getFragmentManager(), "AddInFavorit");
    }

    private void searchUser(final String q, final int offset, final int count) {
        if (isLoad)
            return;
        isLoad = true;
        Thread threadSearch = new Thread(new Runnable() {
            @Override
            public void run() {
                if (((new Date().getTime()) - backTime) < VkApiHelper.SLEEP_TIME) {
                    try {
                        Thread.sleep(VkApiHelper.SLEEP_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                backTime = new Date().getTime();

                VkApiHelper.searchUser(q, offset, count, new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        if (findSV.getQuery().toString().isEmpty()) {
                            clearAllItems();
                            return;
                        }

                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();

                        ModelAnswerUser answer = gson.fromJson(response.json.toString(), ModelAnswerUser.class);

                        boolean isFirst = false;
                        if (size == 0)
                            isFirst = true;

                        for (ModelUserIntro i: answer.getResponse().getItems()) {
                            addItems(i, isFirst);
                        }

                        if (isFirst)
                            runLayoutAnimation(rvFindFragment);


                        if (answer.getResponse().getItems().length == 0)
                            adapter.setEnd(true);
                        size += answer.getResponse().getItems().length;
                        findPB.setVisibility(View.INVISIBLE);
                        isLoad = false;
                    }

                    @Override
                    public void onError(VKError error) {
                        updateAllItems();
                        findPB.setVisibility(View.INVISIBLE);
                        layoutWithBtn.setVisibility(View.VISIBLE);
                        Activity act = getActivity();
                        if (act != null)
                            VkErrorHelper.error(error, act);
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        updateAllItems();
                        findPB.setVisibility(View.INVISIBLE);
                        layoutWithBtn.setVisibility(View.VISIBLE);
                        //Toast.makeText(getActivity(), getActivity().getString(R.string.error) + request.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        threadSearch.setPriority(3);
        threadSearch.start();
    }

    public void addSearch() {
        if (radioUserBtn.isChecked()) {
            searchUser(findSV.getQuery().toString(), size, COUNT_FIND);
        } else if (radioGroupBtn.isChecked()){
            searchGroup(findSV.getQuery().toString(), size, COUNT_FIND);
        }
    }

    private void searchGroup(final String q, final int offset, final int count) {
        if (isLoad)
            return;
        isLoad = true;
        Thread threadSearch = new Thread(new Runnable() {
            @Override
            public void run() {
                if (((new Date().getTime()) - backTime) < VkApiHelper.SLEEP_TIME) {
                    try {
                        Thread.sleep(VkApiHelper.SLEEP_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                backTime = new Date().getTime();

                VkApiHelper.searchGroup(q, offset, count, new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();

                        ModelAnswerGroup answer = gson.fromJson(response.json.toString(), ModelAnswerGroup.class);

                        if (findSV.getQuery().toString().isEmpty()) {
                            clearAllItems();
                            return;
                        }

                        boolean isFirst = false;
                        if (size == 0)
                            isFirst = true;

                        for (ModelGroupIntro i: answer.getResponse().getItems()) {
                            addItems(i, !isFirst);
                        }

                        if (isFirst)
                            runLayoutAnimation(rvFindFragment);

                        if (answer.getResponse().getItems().length == 0 || answer.getResponse().getItems().length < COUNT_FIND)
                            adapter.setEnd(true);

                        size += answer.getResponse().getItems().length;
                        findPB.setVisibility(View.INVISIBLE);
                        isLoad = false;
                    }

                    @Override
                    public void onError(VKError error) {
                        updateAllItems();
                        findPB.setVisibility(View.INVISIBLE);
                        layoutWithBtn.setVisibility(View.VISIBLE);

                        Activity act = getActivity();
                        if (act != null)
                            VkErrorHelper.error(error, act);
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        updateAllItems();
                        findPB.setVisibility(View.INVISIBLE);
                        layoutWithBtn.setVisibility(View.VISIBLE);
                        //Toast.makeText(getActivity(), getActivity().getString(R.string.error) + request.toString()
                        //        + " attemptNumber = " + attemptNumber + " totalAttempts = " + totalAttempts, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        threadSearch.setPriority(3);
        threadSearch.start();
    }

    private void addItems(CustomItemIntro i, boolean isNotify) {
        adapter.addItem(i, isNotify);
    }

    private void updateAllItems() {
        size = 0;
        adapter.clearItems();

        if (!findSV.getQuery().toString().isEmpty()
                && findSV.getQuery().toString().length() >= PreferencesHelper.MIN_LENGTH_FIND) {
            findPB.setVisibility(View.VISIBLE);
            layoutWithBtn.setVisibility(View.INVISIBLE);
            if (radioUserBtn.isChecked()) {
                searchUser(findSV.getQuery().toString(), 0, COUNT_FIND);
            } else if (radioGroupBtn.isChecked()){
                searchGroup(findSV.getQuery().toString(), 0, COUNT_FIND);
            }
        } else {
            findPB.setVisibility(View.INVISIBLE);
            layoutWithBtn.setVisibility(View.VISIBLE);
        }
    }

    public void clear() {
        clearAllItems();
    }

    private class MyScrollListener  extends RecyclerView.OnScrollListener  {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount && dy > 0) {
                userScrolled = false;
                addSearch();
            }
        }
    }
}
