package ru.freshtape.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ru.freshtape.model.ModelGroupIntro;
import ru.freshtape.model.CustomItemIntro;
import ru.freshtape.model.ModelUserIntro;
import ru.freshtape.model.Types;

/**
 * Created by Павел on 07.05.2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "VKTapeDB";

    public static final String TABLE_FAVORITES = "favorites";

    public static final String KEY_ID = "_id";
    public static final String KEY_VK_ID = "vk_id";
    public static final String KEY_TYPE_INT = "type_int";
    public static final String KEY_TYPE = "type";
    public static final String KEY_SHORT_NAME = "short_name";
    public static final String KEY_NAME = "name";
    public static final String KEY_PHOTO_50 = "photo_50",
            KEY_PHOTO_100 = "photo_100",
            KEY_PHOTO_200 = "photo_200";

    public static final String TABLE_VIEWED = "viewed";
    public static final String KEY_VK_FROM_ID = "vk_from_id";
    public static final String KEY_VK_ID_FIRST = "vk_id_first";
    public static final String KEY_VK_ID_SECOND = "vk_id_second";
    //public static final String KEY_VK_FROM_ID_FIRST = "vk_from_id_first";
    //public static final String KEY_VK_FROM_ID_SECOND = "vk_from_id_second";
    public static final String KEY_DATE_FIRST = "date_first";
    public static final String KEY_DATE_SECOND = "date_second";
    //public static final String KEY_FIRST = "first";
    //public static final String KEY_SECOND = "second";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_FAVORITES + "("
                + KEY_ID + " integer primary key autoincrement,"
                + KEY_VK_ID + " integer not null,"
                + KEY_NAME + " text,"
                + KEY_SHORT_NAME + " text,"
                + KEY_TYPE_INT + " integer,"
                + KEY_TYPE + " text,"
                + KEY_PHOTO_50 + " text,"
                + KEY_PHOTO_100 + " text,"
                + KEY_PHOTO_200 + " text" + ")");

        db.execSQL("create table " + TABLE_VIEWED + "("
                + KEY_ID + " integer primary key autoincrement,"
                + KEY_VK_FROM_ID + " integer not null,"

                //+ KEY_FIRST + " integer not null default(0),"
                + KEY_VK_ID_FIRST + " integer not null,"
                + KEY_DATE_FIRST + " integer not null,"

                //+ KEY_SECOND + " integer not null default(0),"
                + KEY_VK_ID_SECOND + " integer not null,"
                + KEY_DATE_SECOND + " integer not null" + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_FAVORITES);
        db.execSQL("drop table if exists " + TABLE_VIEWED);
        onCreate(db);
    }

    public void delete(SQLiteDatabase db) {
        db.execSQL("delete from " + TABLE_FAVORITES);
        db.execSQL("delete from " + TABLE_VIEWED);
    }

    public boolean findItemInFavorites(SQLiteDatabase db, long id) {
        Cursor find = db.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                DBHelper.KEY_VK_ID + " = ?", new String[]{String.valueOf(id)});

        boolean res = find != null && find.getCount() > 0;
        if (find != null)
            find.close();
        return res;
    }

    public void addItem(DBHelper dbHelper, CustomItemIntro customItem,
                        CallbackAddFavorites runnableAddFavorites) {
        ModelGroupIntro modelGroupIntro = null;
        ModelUserIntro modelUserIntro = null;
        if (customItem.getTypeView() == Types.TYPE_GROUP) {
            modelGroupIntro = (ModelGroupIntro) customItem;
        } else if (customItem.getTypeView() == Types.TYPE_USER) {
            modelUserIntro = (ModelUserIntro) customItem;
        }

        SQLiteDatabase readableDB = dbHelper.getReadableDatabase();
        ContentValues cv = new ContentValues();
        Cursor find = null;

        if (customItem.getTypeView() == Types.TYPE_GROUP) {
            find = readableDB.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                    DBHelper.KEY_VK_ID + " = ?", new String[]{String.valueOf(modelGroupIntro.getId())});
        } else if (customItem.getTypeView() == Types.TYPE_USER) {
            find = readableDB.rawQuery("select * from " + DBHelper.TABLE_FAVORITES + " where " +
                    DBHelper.KEY_VK_ID + " = ?", new String[]{String.valueOf(modelUserIntro.getId())});
        }

        SQLiteDatabase writableDB = dbHelper.getWritableDatabase();

        if (dbHelper.findItemInFavorites(find)) {
            long id = -1;
            if (customItem.getTypeView() == Types.TYPE_GROUP) {
                id = modelGroupIntro.getId();
            } else if (customItem.getTypeView() == Types.TYPE_USER) {
                id = modelUserIntro.getId();
            }

            writableDB.delete(DBHelper.TABLE_FAVORITES, DBHelper.KEY_VK_ID + "=" + id, null);
            runnableAddFavorites.del();
        } else {
            if (customItem.getTypeView() == Types.TYPE_GROUP) {
                cv.put(DBHelper.KEY_VK_ID, modelGroupIntro.getId());
                cv.put(DBHelper.KEY_NAME, modelGroupIntro.getName());
                cv.put(DBHelper.KEY_SHORT_NAME, modelGroupIntro.getScreen_name());
                cv.put(DBHelper.KEY_TYPE, modelGroupIntro.getType());
                cv.put(DBHelper.KEY_PHOTO_50, modelGroupIntro.getPhoto_50());
                cv.put(DBHelper.KEY_PHOTO_100, modelGroupIntro.getPhoto_100());
                cv.put(DBHelper.KEY_PHOTO_200, modelGroupIntro.getPhoto_200());
                cv.put(DBHelper.KEY_TYPE_INT, Types.TYPE_GROUP);
            } else if (customItem.getTypeView() == Types.TYPE_USER) {
                cv.put(DBHelper.KEY_VK_ID, modelUserIntro.getId());
                cv.put(DBHelper.KEY_NAME, modelUserIntro.getFirst_name() + " " + modelUserIntro.getLast_name());
                cv.put(DBHelper.KEY_SHORT_NAME, modelUserIntro.getDomain());
                cv.put(DBHelper.KEY_TYPE, "user");
                cv.put(DBHelper.KEY_PHOTO_50, modelUserIntro.getPhoto_50());
                cv.put(DBHelper.KEY_PHOTO_100, modelUserIntro.getPhoto_100());
                cv.put(DBHelper.KEY_PHOTO_200, modelUserIntro.getPhoto_200());
                cv.put(DBHelper.KEY_TYPE_INT, Types.TYPE_USER);
            }

            if (cv.size() != 0 && !dbHelper.findItemInFavorites(find)) {
                writableDB.insert(DBHelper.TABLE_FAVORITES, null, cv);
                runnableAddFavorites.add();
            }
        }

        if (find != null)
            find.close();
    }

    public boolean findItemInFavorites(Cursor find) {
        return find != null && find.getCount() > 0;
    }

    public interface CallbackAddFavorites {
        public abstract void del();
        public abstract void add();
    }
}
