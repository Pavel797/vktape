package ru.freshtape.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.securepreferences.SecurePreferences;

import ru.freshtape.R;

/**
 * Created by Павел on 07.05.2017.
 */
public class PreferencesHelper {

    public static final int MIN_LENGTH_FIND = 1;
    public static final String YM_API_KEY = "aa5cde25-6b9f-4c3d-8710-76c23c214ddb";

    private static final String STORAGE_NAME = "VKTape";
    private static final String FLAG_IS_FIRST_APPLICATION_LAUNCH = "isFirstApplicationLaunch";
    private static final String IS_COLOR_SCANNED_POST = "isColorScannedPosts";
    private static final String IS_OFF_SCANNED_POSTS = "isOffScannedPosts";
    private static final String IS_AILTRATION_BAYANS = "isAiltrationBayansSwitch";
    private static final String USER_ID = "userId";
    private static final String SCROLL_ON_BTN_VOICE = "scrollOnBtnVoiceSwitch";
    private static final String CLOSE_GIF = "CloseGif";
    private static final String FORCE_SCROLL = "forceScroll";
    public static final String FROM_ID = "from_id";
    public static final String ID_POST = "id_post";
    public static final int MAX_FORCE_SCROLL = 500;
    public static final int DEFAULT_FORCE_SCROLL = MAX_FORCE_SCROLL / 2;
    public static final String TYPE = "type";


    private static SharedPreferences settings = null;
    public static Context context = null;

    public static void init(Context cntxt) {
        context = cntxt;
    }

    private static void init() {
        if (settings != null)
            return;

        settings = new SecurePreferences(context, context.getString(R.string.pass_pref), STORAGE_NAME);
    }

    public static boolean isAiltrationBayans() {
        init();
        return settings.getBoolean(IS_AILTRATION_BAYANS, false);
    }

    public static void setAiltrationBayans(boolean val) {
        init();
        addProperty(IS_AILTRATION_BAYANS, val);
    }

    public static boolean isFirstApplicationLaunch() {
        init();
        return settings.getBoolean(FLAG_IS_FIRST_APPLICATION_LAUNCH, true);
    }

    public static boolean isColorScannedPosts() {
        init();
        return settings.getBoolean(IS_COLOR_SCANNED_POST, true);
    }

    public static void setColorScannedPosts(boolean val) {
        init();
        addProperty(IS_COLOR_SCANNED_POST, val);
    }

    public static boolean isOffScannedPosts() {
        init();
        return settings.getBoolean(IS_OFF_SCANNED_POSTS, false);
    }

    public static void setOffScannedPosts(boolean val) {
        init();
        addProperty(IS_OFF_SCANNED_POSTS, val);
    }

    public static void setFirstApplicationLaunch() {
        init();
        addProperty(FLAG_IS_FIRST_APPLICATION_LAUNCH, false);
    }

    private static void addProperty(String name, boolean value) {
        init();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    private static void addProperty(String name, String value) {
        init();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    private static String getProperty(String name) {
        init();
        return settings.getString(name, null);
    }

    public static long getUserId() {
        init();
        return settings.getInt(USER_ID, -1);
    }

    public static void setUserId(long userId) {
        init();
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(USER_ID, userId);
        editor.apply();
    }

    public static boolean isScrollOnBtnVoice() {
        init();
        return settings.getBoolean(SCROLL_ON_BTN_VOICE, false);
    }

    public static void setScrollOnBtnVoice(boolean val) {
        init();
        addProperty(SCROLL_ON_BTN_VOICE, val);
    }

    public static int getForceScroll() {
        init();
        return settings.getInt(FORCE_SCROLL, DEFAULT_FORCE_SCROLL);
    }

    public static boolean isCloseGif() {
        init();
        return settings.getBoolean(CLOSE_GIF, false);
    }

    public static void setForceScroll(int force) {
        init();
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(FORCE_SCROLL, force);
        editor.apply();
    }

    public static void setCloseGif(boolean closeGif) {
        init();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(CLOSE_GIF, closeGif);
        editor.apply();
    }
}