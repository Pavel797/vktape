package ru.freshtape.model;

/**
 * Created by Павел on 11.06.2017.
 */
public class ModelGroup {
    private ModelGroupIntro info;
    private int is_author;

    public ModelGroup() {
    }

    public ModelGroupIntro getInfo() {
        return info;
    }

    public void setInfo(ModelGroupIntro info) {
        this.info = info;
    }

    public int getIs_author() {
        return is_author;
    }

    public void setIs_author(int is_author) {
        this.is_author = is_author;
    }
}
