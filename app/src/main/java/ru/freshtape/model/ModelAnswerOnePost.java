package ru.freshtape.model;

/**
 * Created by Павел on 12.05.2017.
 */

public class ModelAnswerOnePost {

    private ModelResponse response;

    public ModelAnswerOnePost() {}

    public ModelPostWithAuthor getPost() {
        return response.getPost();
    }

    public void setPost(ModelPostWithAuthor post) {
        this.response.setPost(post);
    }

    public SmallModelPost getSecond_post_date() {
        return response.getSecond_post_date();
    }

    public void setSecond_post_date(SmallModelPost second_post_date) {
        this.response.setSecond_post_date(second_post_date);
    }

    public class ModelResponse {
        private ModelPostWithAuthor post;
        private SmallModelPost second_post_date;

        public ModelResponse() {}

        public ModelPostWithAuthor getPost() {
            return post;
        }

        public void setPost(ModelPostWithAuthor post) {
            this.post = post;
        }

        public SmallModelPost getSecond_post_date() {
            return second_post_date;
        }

        public void setSecond_post_date(SmallModelPost second_post_date) {
            this.second_post_date = second_post_date;
        }
    }
}
