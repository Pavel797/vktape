package ru.freshtape.model;

/**
 * Created by Павел on 12.07.2017.
 */

public class Size {
    private int w, h;

    public Size() {
        w = -1;
        h = -1;
    }

    public Size(int x, int h) {
        this.w = x;
        this.h = h;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }
}
