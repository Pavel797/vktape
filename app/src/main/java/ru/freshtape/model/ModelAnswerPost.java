package ru.freshtape.model;

/**
 * Created by Павел on 10.05.2017.
 */

public class ModelAnswerPost {

    private ModelResponse response;

    public ModelAnswerPost() {}

    public ModelResponse getResponse() {
        return response;
    }

    public void setResponse(ModelResponse response) {
        this.response = response;
    }

    public class ModelResponse {

        private long count;
        private ModelPost[] items;
        private ModelUserIntro profiles[];
        private ModelGroupIntro groups[];

        public ModelResponse() {}

        public ModelUserIntro[] getProfiles() {
            return profiles;
        }

        public void setProfiles(ModelUserIntro[] profiles) {
            this.profiles = profiles;
        }

        public ModelGroupIntro[] getGroups() {
            return groups;
        }

        public void setGroups(ModelGroupIntro[] groups) {
            this.groups = groups;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public ModelPost[] getItems() {
            return items;
        }

        public void setItems(ModelPost[] items) {
            this.items = items;
        }
    }
}
