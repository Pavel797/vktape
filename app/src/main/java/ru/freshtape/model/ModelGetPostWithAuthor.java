package ru.freshtape.model;

/**
 * Created by Павел on 11.06.2017.
 */

public class ModelGetPostWithAuthor {

    private Response response;

    public ModelPostWithAuthor getResponse() {
        ModelPostWithAuthor res = new ModelPostWithAuthor();
        if (response.getGroups() != null && response.getGroups().length > 0) {
            ModelGroup modelGroup[] = new ModelGroup[response.getGroups().length];

            for (int i = 0; i < response.getGroups().length; i++) {
                modelGroup[i] = new ModelGroup();
                modelGroup[i].setIs_author(response.getGroups()[i].getIs_author());
                modelGroup[i].setInfo(response.getGroups()[i]);
            }

            res.setGroup(modelGroup);
        }
        if (response.getProfiles() != null && response.getProfiles().length > 0) {
            ModelUser modelUsers[] = new ModelUser[response.getProfiles().length];

            for (int i = 0; i < response.getProfiles().length; i++) {
                modelUsers[i] = new ModelUser();
                modelUsers[i].setIs_author(response.getProfiles()[i].getIs_author());
                modelUsers[i].setInfo(response.getProfiles()[i]);
            }

            res.setProfile(modelUsers);
        }
        res.setWall(response.getWall());
        return res;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public class Response {

        private ModelPost items[];
        private ModelUserIntro profiles[];
        private ModelGroupIntro groups[];

        public Response() {

        }

        public ModelUserIntro[] getProfiles() {
            return profiles;
        }

        public void setProfiles(ModelUserIntro[] profiles) {
            this.profiles = profiles;
        }

        public ModelGroupIntro[] getGroups() {
            return groups;
        }

        public void setGroups(ModelGroupIntro[] groups) {
            this.groups = groups;
        }

        public ModelPost getWall() {
            if (items.length == 0)
                return null;
            return items[0];
        }

        public ModelGroupIntro getAuthorGroup() {
            return getAuthorGroup(0);
        }

        public ModelUserIntro getAuthorUser() {
            return getAuthorUser(0);
        }

        public ModelPost[] getItems() {
            return items;
        }

        public void setItems(ModelPost[] items) {
            this.items = items;
        }

        public ModelGroupIntro getAuthorGroup(int index) {
            if (groups != null)
            for (ModelGroupIntro i : groups) {
                if (i.getIs_author() == 1)
                    return i;
            }
            return null;
        }

        public ModelUserIntro getAuthorUser(int index) {
            if (profiles != null)
            for (ModelUserIntro i : profiles) {
                if (i.getIs_author() == 1)
                    return i;
            }
            return null;
        }
        public long getFrom_id() {
            return items[0].getFrom_id();
        }

        public int getTypeView() {
            return Types.TYPE_WITH_AUTHOR_POST;
        }

        public long getId() {
            return items[0].getId();
        }
    }
}
