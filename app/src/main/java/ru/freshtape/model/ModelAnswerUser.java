package ru.freshtape.model;

/**
 * Created by Павел on 08.05.2017.
 */

public class ModelAnswerUser {

    private ModelResponse response;

    public ModelAnswerUser() {}

    public ModelResponse getResponse() {
        return response;
    }

    public void setResponse(ModelResponse response) {
        this.response = response;
    }

    public class ModelResponse {

        private long count;
        private ModelUserIntro[] items;

        public ModelResponse() {}

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public ModelUserIntro[] getItems() {
            return items;
        }

        public void setItems(ModelUserIntro[] items) {
            this.items = items;
        }
    }
}
