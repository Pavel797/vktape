package ru.freshtape.model;

/**
 * Created by Павел on 08.05.2017.
 */

public class ModelProgressBar implements CustomItemIntro, CustomItemPost {

    @Override
    public long getFrom_id() {
        return -1;
    }

    @Override
    public long getId() {
        return -1;
    }

    @Override
    public boolean isView() {
        return false;
    }

    @Override
    public void setView(boolean val) {
    }

    @Override
    public Author getHistoryAuthor() {
        return null;
    }

    @Override
    public Author getAuthor() {
        return null;
    }

    @Override
    public int getTypeView() {
        return Types.TYPE_PROGERSS_BAR;
    }
}
