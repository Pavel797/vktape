package ru.freshtape.model;

/**
 * Created by Павел on 11.06.2017.
 */

public class ModelUser {
    public ModelUserIntro info;
    public int is_author;

    public ModelUser() {
    }

    public ModelUserIntro getInfo() {
        return info;
    }

    public void setInfo(ModelUserIntro info) {
        this.info = info;
    }

    public int getIs_author() {
        return is_author;
    }

    public void setIs_author(int is_author) {
        this.is_author = is_author;
    }

    public class Profile {
        private long id;
        private int online;
        private String domain;
        private String first_name;
        private String last_name;
        private String type;
        private String screen_name;
        private String photo_50, photo_100, photo_200, photo_max, photo_200_orig, photo_400_orig;
        public int is_author;

        public Profile() {

        }

        public int getIs_author() {
            return is_author;
        }

        public void setIs_author(int is_author) {
            this.is_author = is_author;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public int getOnline() {
            return online;
        }

        public void setOnline(int online) {
            this.online = online;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getScreen_name() {
            return screen_name;
        }

        public void setScreen_name(String screen_name) {
            this.screen_name = screen_name;
        }

        public String getPhoto_50() {
            return photo_50;
        }

        public void setPhoto_50(String photo_50) {
            this.photo_50 = photo_50;
        }

        public String getPhoto_100() {
            return photo_100;
        }

        public void setPhoto_100(String photo_100) {
            this.photo_100 = photo_100;
        }

        public String getPhoto_200() {
            return photo_200;
        }

        public void setPhoto_200(String photo_200) {
            this.photo_200 = photo_200;
        }

        public String getPhoto_max() {
            return photo_max;
        }

        public void setPhoto_max(String photo_max) {
            this.photo_max = photo_max;
        }

        public String getPhoto_200_orig() {
            return photo_200_orig;
        }

        public void setPhoto_200_orig(String photo_200_orig) {
            this.photo_200_orig = photo_200_orig;
        }

        public String getPhoto_400_orig() {
            return photo_400_orig;
        }

        public void setPhoto_400_orig(String photo_400_orig) {
            this.photo_400_orig = photo_400_orig;
        }
    }
}
