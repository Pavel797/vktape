package ru.freshtape.model;

/**
 * Created by Павел on 09.05.2017.
 */

    public class ModelAnswerUserSearch {

    private ModelUserIntro[] response;

    public ModelAnswerUserSearch() {}

    public ModelUserIntro[] getResponse() {
        return response;
    }

    public void setResponse(ModelUserIntro[] response) {
        this.response = response;
    }
}
