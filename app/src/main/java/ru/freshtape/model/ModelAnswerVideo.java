package ru.freshtape.model;

/**
 * Created by Павел on 16.05.2017.
 */

public class ModelAnswerVideo {
    private ModelResponse response;

    public ModelAnswerVideo() {}

    public ModelResponse getResponse() {
        return response;
    }

    public void setResponse(ModelResponse response) {
        this.response = response;
    }

    public class ModelResponse {

        private long count;
        private ModelPost.Attachments.Video[] items;

        public ModelResponse() {}

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public ModelPost.Attachments.Video[] getItems() {
            return items;
        }

        public void setItems(ModelPost.Attachments.Video[] items) {
            this.items = items;
        }
    }
}
