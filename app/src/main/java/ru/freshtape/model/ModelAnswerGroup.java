package ru.freshtape.model;

/**
 * Created by Павел on 08.05.2017.
 */

public class ModelAnswerGroup {

    private ModelResponse response;

    public ModelAnswerGroup() {}

    public ModelResponse getResponse() {
        return response;
    }

    public void setResponse(ModelResponse response) {
        this.response = response;
    }

    public class ModelResponse {

        private long count;
        private ModelGroupIntro[] items;

        public ModelResponse() {}

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public ModelGroupIntro[] getItems() {
            return items;
        }

        public void setItems(ModelGroupIntro[] items) {
            this.items = items;
        }
    }
}
