package ru.freshtape.model;

/**
 * Created by Павел on 12.05.2017.
 */

public class SmallModelPost implements CustomItemPost {
    private long id;
    private long from_id;
    private long date;
    private boolean isView;

    public SmallModelPost() {}

    public SmallModelPost(long from_id, long date, long id) {
        this.from_id = from_id;
        this.date = date;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean isView() {
        return isView;
    }

    @Override
    public void setView(boolean val) {
        this.isView = val;
    }

    @Override
    public Author getHistoryAuthor() {
        return null;
    }

    @Override
    public Author getAuthor() {
        return null;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int getTypeView() {
        return Types.TYPE_SMALL_POST;
    }

    public long getFrom_id() {
        return from_id;
    }

    public void setFrom_id(long from_id) {
        this.from_id = from_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}

    /*public class ModelResponse {

        private long count;
        private SmallModelPost[] items;

        public ModelResponse() {}

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public SmallModelPost[] getItems() {
            return items;
        }

        public void setItems(SmallModelPost[] items) {
            this.items = items;
        }

        public class SmallModelPost {
            private long from_id;
            private long date;

            public SmallModelPost() {}

            public long getFrom_id() {
                return from_id;
            }

            public void setFrom_id(long from_id) {
                this.from_id = from_id;
            }

            public long getDate() {
                return date;
            }

            public void setDate(long date) {
                this.date = date;
            }
        }
    }*/
