package ru.freshtape.model;

/**
 * Created by Павел on 09.05.2017.
 */

public class ModelAnswerGroupSearch {

    private ModelGroupIntro[] response;

    public ModelAnswerGroupSearch() {}

    public ModelGroupIntro[] getResponse() {
        return response;
    }

    public void setResponse(ModelGroupIntro[] response) {
        this.response = response;
    }
}
