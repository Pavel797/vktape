package ru.freshtape.model;

/**
 * Created by Павел on 12.06.2017.
 */

public interface CustomItemPost {
    int getTypeView();
    long getFrom_id();
    long getId();
    boolean isView();
    void setView(boolean val);
    Author getHistoryAuthor();
    Author getAuthor();
}
