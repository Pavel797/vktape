package ru.freshtape.model;

/**
 * Created by Павел on 01.07.2017.
 */

public class ModelAnswerSearchItem {

    private Response response;

    public ModelAnswerSearchItem() {}

    public int getType() {
        return response.getType();
    }

    public void setType(int type) {
        this.response.setType(type);
    }

    public ModelUserIntro getModelUserIntro() {
        return response.getModelUserIntro();
    }

    public void setModelUserIntro(ModelUserIntro modelUserIntro) {
        this.response.setModelUserIntro(modelUserIntro);
    }

    public ModelGroupIntro getModelGroupIntro() {
        return response.getModelGroupIntro();
    }

    public void setModelGroupIntro(ModelGroupIntro modelGroupIntro) {
        this.response.setModelGroupIntro(modelGroupIntro);
    }

    private class Response {
        private int type;
        private ModelUserIntro modelUserIntro;
        private ModelGroupIntro modelGroupIntro;

        public Response() {}

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public ModelUserIntro getModelUserIntro() {
            return modelUserIntro;
        }

        public void setModelUserIntro(ModelUserIntro modelUserIntro) {
            this.modelUserIntro = modelUserIntro;
        }

        public ModelGroupIntro getModelGroupIntro() {
            return modelGroupIntro;
        }

        public void setModelGroupIntro(ModelGroupIntro modelGroupIntro) {
            this.modelGroupIntro = modelGroupIntro;
        }
    }
}
