package ru.freshtape.model;

/**
 * Created by Павел on 17.06.2017.
 */

public interface Author {
    int getIntType();
    String getName();
    String getPhoto_50();
    String getPhoto_100();
    String getPhoto_200();
}
