package ru.freshtape.model;

/**
 * Created by Павел on 08.05.2017.
 */

public class ModelGroupIntro implements CustomItemIntro, Author{

    private long id;
    private String name;
    private String screen_name;
    private int is_closed;
    private String type;
    private int is_admin;
    private int is_member;
    private String photo_50, photo_100, photo_200;
    private int is_author;

    @Override
    public int getIntType() {
        return Types.TYPE_GROUP;
    }

    public ModelGroupIntro() {}

    public int getIs_author() {
        return is_author;
    }

    public void setIs_author(int is_author) {
        this.is_author = is_author;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public int getIs_closed() {
        return is_closed;
    }

    public void setIs_closed(int is_closed) {
        this.is_closed = is_closed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(int is_admin) {
        this.is_admin = is_admin;
    }

    public int getIs_member() {
        return is_member;
    }

    public void setIs_member(int is_member) {
        this.is_member = is_member;
    }

    @Override
    public String getPhoto_50() {
        return photo_50;
    }

    public void setPhoto_50(String photo_50) {
        this.photo_50 = photo_50;
    }

    @Override
    public String getPhoto_100() {
        return photo_100;
    }

    public void setPhoto_100(String photo_100) {
        this.photo_100 = photo_100;
    }

    @Override
    public String getPhoto_200() {
        return photo_200;
    }

    public void setPhoto_200(String photo_200) {
        this.photo_200 = photo_200;
    }

    public long getId() {
        if (id > 0)
            return -id;
        return id;
    }

    @Override
    public int getTypeView() {
        return Types.TYPE_GROUP;
    }
}
