package ru.freshtape.model;

/**
 * Created by Павел on 11.05.2017.
 */

public class ModelAnswerFirstPosts {
    private SmallModelPost response[];

    public ModelAnswerFirstPosts() {}

    public SmallModelPost[] getResponse() {
        return response;
    }

    public void setResponse(SmallModelPost[] response) {
        this.response = response;
    }
}
