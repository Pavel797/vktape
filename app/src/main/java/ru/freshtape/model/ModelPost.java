package ru.freshtape.model;

/**
 * Created by Павел on 07.05.2017.
 */

public class ModelPost {

    private long id;
    private long from_id;
    private long owner_id;
    private long date;
    private String post_type;
    private String text;
    private int can_edit;
    private int can_delete;
    private int can_pin;
    private int is_pinned;
    private Attachments attachments[];
    private Post_source post_source;
    private Comments comments;
    private Likes likes;
    private Reposts reposts;
    private Views views;
    private ModelPost copy_history[];

    public ModelPost() {
    }

    public int getIs_pinned() {
        return is_pinned;
    }

    public void setIs_pinned(int is_pinned) {
        this.is_pinned = is_pinned;
    }

    public ModelPost[] getCopy_history() {
        return copy_history;
    }

    public void setCopy_history(ModelPost[] copy_history) {
        this.copy_history = copy_history;
    }

    public long getFrom_id() {
        return from_id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFrom_id(long from_id) {
        this.from_id = from_id;
    }

    public long getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(long owner_id) {
        this.owner_id = owner_id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCan_edit() {
        return can_edit;
    }

    public void setCan_edit(int can_edit) {
        this.can_edit = can_edit;
    }

    public int getCan_delete() {
        return can_delete;
    }

    public void setCan_delete(int can_delete) {
        this.can_delete = can_delete;
    }

    public int getCan_pin() {
        return can_pin;
    }

    public void setCan_pin(int can_pin) {
        this.can_pin = can_pin;
    }

    public Attachments[] getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachments[] attachments) {
        this.attachments = attachments;
    }

    public Post_source getPost_source() {
        return post_source;
    }

    public void setPost_source(Post_source post_source) {
        this.post_source = post_source;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public Reposts getReposts() {
        return reposts;
    }

    public void setReposts(Reposts reposts) {
        this.reposts = reposts;
    }

    public Views getViews() {
        return views;
    }

    public void setViews(Views views) {
        this.views = views;
    }

    /*@Override
    public int getTypeView() {
        return Types.TYPE_POST;
    }*/

    public long getId() {
        return id;
    }

    public boolean isHaveHistory() {
        return copy_history != null && copy_history.length > 0;
    }

    public class Attachments {

        public Size size;

        private String type;
        private Photo photo;
        private Link link;
        private Video video;
        private Doc doc;
        private Page page;

        public Attachments() {
        }

        public Page getPage() {
            return page;
        }

        public void setPage(Page page) {
            this.page = page;
        }

        public Link getLink() {
            return link;
        }

        public void setLink(Link link) {
            this.link = link;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Photo getPhoto() {
            return photo;
        }

        public void setPhoto(Photo photo) {
            this.photo = photo;
        }

        public Video getVideo() {
            return video;
        }

        public void setVideo(Video video) {
            this.video = video;
        }

        public Doc getDoc() {
            return doc;
        }

        public void setDoc(Doc doc) {
            this.doc = doc;
        }

        public class Page {

            private long id;
            private long group_id;
            private long creator_id;
            private String view_url;
            private String title;

            public Page() {
            }

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public long getGroup_id() {
                return group_id;
            }

            public void setGroup_id(long group_id) {
                this.group_id = group_id;
            }

            public long getCreator_id() {
                return creator_id;
            }

            public void setCreator_id(long creator_id) {
                this.creator_id = creator_id;
            }

            public String getView_url() {
                return view_url;
            }

            public void setView_url(String view_url) {
                this.view_url = view_url;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public class Link {
            private String url;
            private String title;

            public Link() {
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public class Photo {

            private long id;
            private long album_id;
            private long owner_id;
            private String photo_75, photo_130, photo_604, photo_807, photo_1280, photo_2560;
            private int width, height;
            private String text;
            private long date;
            private String access_key;

            public Photo() {
            }

            public String getMaxSizePhoto() {
                if (photo_2560 != null && !photo_2560.isEmpty())
                    return photo_2560;

                if (photo_1280 != null && !photo_1280.isEmpty())
                    return photo_1280;

                if (photo_807 != null && !photo_807.isEmpty())
                    return photo_807;

                if (photo_604 != null && !photo_604.isEmpty())
                    return photo_604;

                if (photo_130 != null && !photo_130.isEmpty())
                    return photo_130;

                return photo_75;
            }

            public String getPhoto_2560() {
                return photo_2560;
            }

            public void setPhoto_2560(String photo_2560) {
                this.photo_2560 = photo_2560;
            }

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public long getAlbum_id() {
                return album_id;
            }

            public void setAlbum_id(long album_id) {
                this.album_id = album_id;
            }

            public long getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(long owner_id) {
                this.owner_id = owner_id;
            }

            public String getPhoto_75() {
                return photo_75;
            }

            public void setPhoto_75(String photo_75) {
                this.photo_75 = photo_75;
            }

            public String getPhoto_130() {
                return photo_130;
            }

            public void setPhoto_130(String photo_130) {
                this.photo_130 = photo_130;
            }

            public String getPhoto_604() {
                return photo_604;
            }

            public void setPhoto_604(String photo_604) {
                this.photo_604 = photo_604;
            }

            public String getPhoto_807() {
                return photo_807;
            }

            public void setPhoto_807(String photo_807) {
                this.photo_807 = photo_807;
            }

            public String getPhoto_1280() {
                return photo_1280;
            }

            public void setPhoto_1280(String photo_1280) {
                this.photo_1280 = photo_1280;
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            public int getHeight() {
                return height;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public long getDate() {
                return date;
            }

            public void setDate(long date) {
                this.date = date;
            }

            public String getAccess_key() {
                return access_key;
            }

            public void setAccess_key(String access_key) {
                this.access_key = access_key;
            }
        }

        public class Video {

            private long id;
            private long owner_id;
            private String title;
            private long duration;
            private String description;
            private long date;
            private long comments;
            private long views;
            private String photo_130, photo_320, photo_800;
            private String access_key;
            private int can_add;
            private String player;
            private Files files;

            public Video() {
            }

            public Files getFiles() {
                return files;
            }

            public void setFiles(Files files) {
                this.files = files;
            }

            public int getCan_add() {
                return can_add;
            }

            public String getPlayer() {
                return player;
            }

            public void setPlayer(String player) {
                this.player = player;
            }

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public long getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(long owner_id) {
                this.owner_id = owner_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public long getDuration() {
                return duration;
            }

            public void setDuration(long duration) {
                this.duration = duration;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public long getDate() {
                return date;
            }

            public void setDate(long date) {
                this.date = date;
            }

            public long getComments() {
                return comments;
            }

            public void setComments(long comments) {
                this.comments = comments;
            }

            public long getViews() {
                return views;
            }

            public void setViews(long views) {
                this.views = views;
            }

            public String getPhoto_130() {
                return photo_130;
            }

            public void setPhoto_130(String photo_130) {
                this.photo_130 = photo_130;
            }

            public String getPhoto_320() {
                return photo_320;
            }

            public void setPhoto_320(String photo_320) {
                this.photo_320 = photo_320;
            }

            public String getPhoto_800() {
                return photo_800;
            }

            public void setPhoto_800(String photo_800) {
                this.photo_800 = photo_800;
            }

            public String getAccess_key() {
                return access_key;
            }

            public void setAccess_key(String access_key) {
                this.access_key = access_key;
            }

            public int isCan_add() {
                return can_add;
            }

            public void setCan_add(int can_add) {
                this.can_add = can_add;
            }
        }

        public class Files {

            String mp4_240, mp4_360, mp4_480, mp4_720;

            Files() {
            }

            public String getMp4_240() {
                return mp4_240;
            }

            public void setMp4_240(String mp4_240) {
                this.mp4_240 = mp4_240;
            }

            public String getMp4_360() {
                return mp4_360;
            }

            public void setMp4_360(String mp4_360) {
                this.mp4_360 = mp4_360;
            }

            public String getMp4_480() {
                return mp4_480;
            }

            public void setMp4_480(String mp4_480) {
                this.mp4_480 = mp4_480;
            }

            public String getMp4_720() {
                return mp4_720;
            }

            public void setMp4_720(String mp4_720) {
                this.mp4_720 = mp4_720;
            }
        }

        public class Doc {

            private long id;
            private long owner_id;
            private String title;
            private long size; // вес документа
            private String ext;
            private String url;
            private String date;
            private int type;
            //private String photo_100, photo_130;
            private Preview preview;
            private String access_key;

            public Doc() {
            }

            public Preview getPreview() {
                return preview;
            }

            public void setPreview(Preview preview) {
                this.preview = preview;
            }

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public long getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(long owner_id) {
                this.owner_id = owner_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public long getSize() {
                return size;
            }

            public void setSize(long size) {
                this.size = size;
            }

            public String getExt() {
                return ext;
            }

            public void setExt(String ext) {
                this.ext = ext;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            /*public Preview getPreview() {
                return preview;
            }

            public void setPreview(Preview preview) {
                this.preview = preview;
            }
            */
            public String getAccess_key() {
                return access_key;
            }

            public void setAccess_key(String access_key) {
                this.access_key = access_key;
            }

            public class Preview {

                private Preview.PhotoD photo;
                private Preview.VideoD video;

                public Preview() {
                }

                public Preview.PhotoD getPhoto() {
                    return photo;
                }

                public void setPhoto(PhotoD photo) {
                    this.photo = photo;
                }

                public VideoD getVideo() {
                    return video;
                }

                public void setVideo(VideoD video) {
                    this.video = video;
                }

                public class PhotoD {

                    private Sizes sizes[];

                    public PhotoD() {
                    }

                    public Sizes[] getSizes() {
                        return sizes;
                    }

                    public void setSizes(Sizes[] sizes) {
                        this.sizes = sizes;
                    }

                    public class Sizes {
                        private String src;
                        private int width, height;
                        private String type;

                        public Sizes() {
                        }

                        public String getSrc() {
                            return src;
                        }

                        public void setSrc(String src) {
                            this.src = src;
                        }

                        public int getWidth() {
                            return width;
                        }

                        public void setWidth(int width) {
                            this.width = width;
                        }

                        public int getHeight() {
                            return height;
                        }

                        public void setHeight(int height) {
                            this.height = height;
                        }

                        public String getType() {
                            return type;
                        }

                        public void setType(String type) {
                            this.type = type;
                        }
                    }
                }

                public class VideoD {

                    private String src;
                    private int width, height;
                    private long file_size;

                    public VideoD() {
                    }

                    public String getSrc() {
                        return src;
                    }

                    public void setSrc(String src) {
                        this.src = src;
                    }

                    public int getWidth() {
                        return width;
                    }

                    public void setWidth(int width) {
                        this.width = width;
                    }

                    public int getHeight() {
                        return height;
                    }

                    public void setHeight(int height) {
                        this.height = height;
                    }

                    public long getFile_size() {
                        return file_size;
                    }

                    public void setFile_size(long file_size) {
                        this.file_size = file_size;
                    }
                }
            }
        }
    }

    public class Post_source {

        private String type;
        private String platform;
        private String data;
        private String url;

        public Post_source() {
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPlatform() {
            return platform;
        }

        public void setPlatform(String platform) {
            this.platform = platform;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public class Comments {

        private long count;
        private int can_post;

        public Comments() {
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public int getCan_post() {
            return can_post;
        }

        public void setCan_post(int can_post) {
            this.can_post = can_post;
        }
    }

    public class Likes {

        private long count;
        private int user_likes;
        private int can_like;
        private int can_publish;

        public Likes() {
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public int getUser_likes() {
            return user_likes;
        }

        public void setUser_likes(int user_likes) {
            this.user_likes = user_likes;
        }

        public int getCan_like() {
            return can_like;
        }

        public void setCan_like(int can_like) {
            this.can_like = can_like;
        }

        public int getCan_publish() {
            return can_publish;
        }

        public void setCan_publish(int can_publish) {
            this.can_publish = can_publish;
        }
    }

    public class Reposts {

        private long count;
        private int user_reposted;

        public Reposts() {
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public int getUser_reposted() {
            return user_reposted;
        }

        public void setUser_reposted(int user_reposted) {
            this.user_reposted = user_reposted;
        }
    }

    public class Views {

        private long count;

        public Views() {
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }
    }
}
