package ru.freshtape.model;

/**
 * Created by Павел on 13.05.2017.
 */

public class ModelPostWithAuthor implements CustomItemPost {

    private ModelPost wall;
    private ModelUser profile[];
    private ModelGroup group[];
    private boolean isView;

    public ModelPostWithAuthor() {
    }

    public ModelPost getWall() {
        return wall;
    }


    public ModelUser[] getProfile() {
        return profile;
    }

    public void setProfile(ModelUser[] profile) {
        this.profile = profile;
    }

    public ModelGroup[] getGroup() {
        return group;
    }

    public ModelGroupIntro getAuthorGroup() {
        for (ModelGroup i : group) {
            if (i.getInfo().getId() == wall.getFrom_id())
                return i.getInfo();
        }
        return null;
    }

    public ModelGroupIntro getHistoryAuthorGroup() {
        if (!getWall().isHaveHistory())
            return null;
        for (ModelGroup i : group) {
            if (i.getInfo().getId() == wall.getCopy_history()[0].getFrom_id())
                return i.getInfo();
        }
        return null;
    }

    public ModelUserIntro getHistoryAuthorUser() {
        if (!getWall().isHaveHistory())
            return null;
        if (profile != null)
            for (ModelUser i : profile) {
                if (i.getInfo().getId() == wall.getCopy_history()[0].getFrom_id())
                    return i.getInfo();
            }
        return null;
    }

    public ModelUserIntro getAuthorUser() {
        if (profile != null)
        for (ModelUser i : profile) {
            if (i.getInfo().getId() == wall.getFrom_id())
                return i.getInfo();
        }
        return null;
    }

    public void setGroup(ModelGroup[] group) {
        this.group = group;
    }

    @Override
    public long getFrom_id() {
        return wall.getFrom_id();
    }

    @Override
    public int getTypeView() {
        return Types.TYPE_WITH_AUTHOR_POST;
    }

    @Override
    public long getId() {
        return wall.getId();
    }

    @Override
    public boolean isView() {
        return isView;
    }

    @Override
    public void setView(boolean val) {
        this.isView = val;
    }

    @Override
    public Author getHistoryAuthor() {
        return getHistoryAuthorUser() == null
                ? getHistoryAuthorGroup() : getHistoryAuthorUser();
    }

    @Override
    public Author getAuthor() {
        return getAuthorUser() == null
                ? getAuthorGroup() : getAuthorUser();
    }

    public void setWall(ModelPost wall) {
        this.wall = wall;
    }
}
