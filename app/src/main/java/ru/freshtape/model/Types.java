package ru.freshtape.model;

/**
 * Created by Павел on 08.05.2017.
 */

public class Types {

    public static final int TYPE_ERROR = -1;
    public static final int TYPE_GROUP = 1;
    public static final int TYPE_USER = 2;
    public static final int TYPE_PROGERSS_BAR = 3;
    public static final int TYPE_POST = 4;
    public static final int TYPE_WITH_AUTHOR_POST = 5;
    public static final int TYPE_SMALL_POST = 6;

    public static final String type_photo = "photo";
    public static final String type_video = "video";
    public static final String type_doc = "doc";
    public static final String type_link = "link";
    public static final String type_page = "page";

    private Types() {}

    public static class TypesDoc {
        public static final int TYPE_TEXT = 1;
        public static final int TYPE_GIF = 3;

        public TypesDoc() {}
    }
}
