package ru.freshtape.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatDrawableManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.freshtape.activities.ViewWallActivity;
import ru.freshtape.model.ModelAnswerSearchItem;
import ru.freshtape.model.ModelGroupIntro;
import ru.freshtape.R;
import ru.freshtape.db.DBHelper;
import ru.freshtape.model.CustomItemIntro;
import ru.freshtape.model.ModelUserIntro;
import ru.freshtape.model.Types;
import ru.freshtape.vk.VkApiHelper;


/**
 * Created by Павел on 08.05.2017.
 */

public class DialogAddInFavorit extends DialogFragment {

    private View content;
    private TextInputLayout linkTIL;
    private EditText editText;
    private Button searchOnReferenceBtn, addDelBtn, cancelBan;
    private ProgressBar progressBar, progresAddDelBtn;
    private LinearLayout contentIntroLL;
    private TextView nameTV, typeTV, isThisTV;
    private CircleImageView profilPhotoIV;

    private DBHelper dbHelper;

    private ModelGroupIntro groupIntro = null;
    private ModelUserIntro userIntro = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        content = inflater.inflate(R.layout.dialog_add_in_favorit, null);

        linkTIL = (TextInputLayout) content.findViewById(R.id.linkTIL);
        editText = linkTIL.getEditText();
        searchOnReferenceBtn = (Button) content.findViewById(R.id.searchOnLinkBtn);
        progressBar = (ProgressBar) content.findViewById(R.id.findPB);
        progressBar.setVisibility(View.GONE);
        progresAddDelBtn = (ProgressBar) content.findViewById(R.id.progresAddDelBtn);
        progresAddDelBtn.setVisibility(View.INVISIBLE);
        contentIntroLL = (LinearLayout) content.findViewById(R.id.contentIntroLL);
        contentIntroLL.setVisibility(View.GONE);
        nameTV = (TextView) content.findViewById(R.id.nameTV);
        typeTV = (TextView) content.findViewById(R.id.typeTV);
        profilPhotoIV = (CircleImageView) content.findViewById(R.id.profilPhotoIV);
        cancelBan = (Button) content.findViewById(R.id.cancelBtn);
        isThisTV = (TextView) content.findViewById(R.id.isThisTV);
        addDelBtn = (Button) content.findViewById(R.id.addDelBtn);

        builder.setView(content);

        dbHelper = new DBHelper(getActivity());

        final Dialog dialog = builder.create();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.addDelBtn:
                        addDelItem();
                        break;
                    case R.id.radioGroupBtn:
                        setUIDefault();
                        break;
                    case R.id.radioUserBtn:
                        setUIDefault();
                        break;
                    case R.id.searchOnLinkBtn:
                        progressBar.setVisibility(View.VISIBLE);
                        if (editText.getText().toString().isEmpty()) {
                            errorSearch();
                        } else {
                            search();
                        }
                        break;
                    case R.id.cancelBtn:
                        dialog.cancel();
                        break;
                }
            }
        };

        addDelBtn.setOnClickListener(listener);
        searchOnReferenceBtn.setOnClickListener(listener);
        cancelBan.setOnClickListener(listener);

        View.OnClickListener listenerGoView = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ViewWallActivity.class);
                if (/*radioGroupBtn.isChecked() &&*/ groupIntro != null) {
                    intent.putExtra(DBHelper.KEY_VK_ID, groupIntro.getId());
                    intent.putExtra(DBHelper.KEY_NAME, groupIntro.getName());
                    intent.putExtra(DBHelper.KEY_SHORT_NAME, groupIntro.getScreen_name());
                    intent.putExtra(DBHelper.KEY_TYPE, groupIntro.getType());
                    intent.putExtra(DBHelper.KEY_PHOTO_50, groupIntro.getPhoto_50());
                    intent.putExtra(DBHelper.KEY_PHOTO_100, groupIntro.getPhoto_100());
                    intent.putExtra(DBHelper.KEY_PHOTO_200, groupIntro.getPhoto_200());
                    intent.putExtra(DBHelper.KEY_TYPE_INT, Types.TYPE_GROUP);
                } else if (/*radioUserBtn.isChecked() &&*/ userIntro != null) {
                    intent.putExtra(DBHelper.KEY_VK_ID, userIntro.getId());
                    intent.putExtra(DBHelper.KEY_NAME, userIntro.getFirst_name() + " " + userIntro.getLast_name());
                    intent.putExtra(DBHelper.KEY_SHORT_NAME, userIntro.getDomain());
                    intent.putExtra(DBHelper.KEY_TYPE, userIntro.getType());
                    intent.putExtra(DBHelper.KEY_PHOTO_50, userIntro.getPhoto_50());
                    intent.putExtra(DBHelper.KEY_PHOTO_100, userIntro.getPhoto_100());
                    intent.putExtra(DBHelper.KEY_PHOTO_200, userIntro.getPhoto_200());
                    intent.putExtra(DBHelper.KEY_TYPE_INT, Types.TYPE_USER);
                }
                getActivity().startActivity(intent);
            }
        };

        contentIntroLL.setOnClickListener(listenerGoView);
        profilPhotoIV.setOnClickListener(listenerGoView);
        
        return dialog;
    }

    @Override
    public void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }

    private void addDelItem() {
        progresAddDelBtn.setVisibility(View.VISIBLE);
        addDelBtn.setVisibility(View.INVISIBLE);

        CustomItemIntro customItem = null;

        if (/*radioGroupBtn.isChecked() &&*/ groupIntro != null) {
            customItem = groupIntro;
        } else if (/*radioUserBtn.isChecked() &&*/ userIntro != null) {
            customItem = userIntro;
        }

        dbHelper.addItem(dbHelper, customItem,
                new DBHelper.CallbackAddFavorites() {
                    @Override
                    public void del() {
                        addDelBtn.setText(content.getContext().getString(R.string.add));
                        addDelBtn.setBackground(AppCompatDrawableManager.get().getDrawable(getActivity(), R.drawable.ok_button_style));
                    }

                    @Override
                    public void add() {
                        addDelBtn.setText(content.getContext().getString(R.string.del));
                        addDelBtn.setBackground(AppCompatDrawableManager.get().getDrawable(getActivity(), R.drawable.cancel_button_style));
                    }
                });

        progresAddDelBtn.setVisibility(View.INVISIBLE);
        addDelBtn.setVisibility(View.VISIBLE);
    }

    private void setUIDefault() {
        progressBar.setVisibility(View.GONE);
        contentIntroLL.setVisibility(View.GONE);
        linkTIL.setError(null);
        searchOnReferenceBtn.setText(getActivity().getString(R.string.search));
        searchOnReferenceBtn.setBackground(AppCompatDrawableManager.get().getDrawable(getActivity(), R.drawable.ok_button_style));
    }

    private void errorSearch() {
        progressBar.setVisibility(View.GONE);
        contentIntroLL.setVisibility(View.GONE);

        groupIntro = null;
        userIntro = null;

        linkTIL.setError(getActivity().getString(R.string.failed_search));

        searchOnReferenceBtn.setText(getActivity().getString(R.string.again));
        searchOnReferenceBtn.setBackground(AppCompatDrawableManager.get().getDrawable(getActivity(), R.drawable.cancel_button_style));
    }

    private void search() {
        isThisTV.setText(getActivity().getString(R.string.this_item));

        VkApiHelper.searchItems(editText.getText().toString(), new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                ModelAnswerSearchItem answer = gson.fromJson(response.json.toString(), ModelAnswerSearchItem.class);


                if (answer.getType() == Types.TYPE_USER) {
                    userIntro = answer.getModelUserIntro();

                    if (dbHelper.findItemInFavorites(dbHelper.getReadableDatabase(), userIntro.getId())) {
                        setDelStyleBtn();
                    } else {
                        setAddStyleBtn();
                    }

                    nameTV.setText(userIntro.getFirst_name() + " " + userIntro.getLast_name());
                    typeTV.setText(userIntro.getDomain());

                    Picasso.with(getActivity())
                            .load(userIntro.getPhoto_50())
                            .fit()
                            .placeholder(R.drawable.circle)
                            .error(R.drawable.circle)
                            .into(profilPhotoIV);
                } else if (answer.getType() == Types.TYPE_GROUP) {
                    groupIntro = answer.getModelGroupIntro();

                    if (dbHelper.findItemInFavorites(dbHelper.getReadableDatabase(), groupIntro.getId())) {
                        setDelStyleBtn();
                    } else {
                        setAddStyleBtn();
                    }

                    nameTV.setText(groupIntro.getName());
                    typeTV.setText(groupIntro.getType());

                    Picasso.with(getActivity())
                            .load(groupIntro.getPhoto_50())
                            .fit()
                            .placeholder(R.drawable.circle)
                            .error(R.drawable.circle)
                            .into(profilPhotoIV);
                } else {
                    errorSearch();
                    return;
                }




                progressBar.setVisibility(View.GONE);
                contentIntroLL.setVisibility(View.VISIBLE);
                linkTIL.setError(null);
                searchOnReferenceBtn.setText(getActivity().getString(R.string.again));
                searchOnReferenceBtn.setBackground(AppCompatDrawableManager.get().getDrawable(getActivity(), R.drawable.ok_button_style));
            }

            @Override
            public void onError(VKError error) {
                errorSearch();
                //VkErrorHelper.error(error, getActivity());
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                groupIntro = null;
                userIntro = null;

                Toast.makeText(getActivity(), getActivity().getString(R.string.error) + request.toString(), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setAddStyleBtn() {
        addDelBtn.setText(getActivity().getString(R.string.add));
        addDelBtn.setBackground(AppCompatDrawableManager.get().getDrawable(getActivity(), R.drawable.ok_button_style));
    }

    private void setDelStyleBtn() {
        addDelBtn.setText(getActivity().getString(R.string.del));
        addDelBtn.setBackground(AppCompatDrawableManager.get().getDrawable(getActivity(), R.drawable.cancel_button_style));
    }
}
