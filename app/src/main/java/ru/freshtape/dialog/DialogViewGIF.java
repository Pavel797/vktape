package ru.freshtape.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.MediaController;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.R;

/**
 * Created by Павел on 17.05.2017.
 */

public class DialogViewGIF extends DialogFragment {

    private View content;
    private String url;
    private Button cancelBtn;
    private ProgressBar progressBar;
    /*private VideoView simpleVideoView;
    private MediaController mediaControls;*/

    private static final String TAG = "MainActivity";
    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer player;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        content = inflater.inflate(R.layout.dialog_view_gif, null);

        cancelBtn = (Button) content.findViewById(R.id.cancelBtn);
        progressBar = (ProgressBar) content.findViewById(R.id.progressBar);
        //simpleVideoView = (VideoView) content.findViewById(R.id.simpleVideoView);

        builder.setView(content);

        if (url != null && !url.isEmpty()) {

// 1. Create a default TrackSelector
            Handler mainHandler = new Handler();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(mainHandler, videoTrackSelectionFactory);

// 2. Create a default LoadControl
            LoadControl loadControl = new DefaultLoadControl();

// 3. Create the player
            player = ExoPlayerFactory.newSimpleInstance(content.getContext(), trackSelector, loadControl);
            simpleExoPlayerView = new SimpleExoPlayerView(content.getContext());
            simpleExoPlayerView = (SimpleExoPlayerView) content.findViewById(R.id.player_view);

//Set media controller
            simpleExoPlayerView.setUseController(false);
            SurfaceView surfaceView = (SurfaceView) simpleExoPlayerView.getVideoSurfaceView();
            surfaceView.setZOrderOnTop(true);

            simpleExoPlayerView.requestFocus();

// Bind the player to the view.
            simpleExoPlayerView.setPlayer(player);

            Uri mp4VideoUri = Uri.parse(url);

// Measures bandwidth during playback. Can be null if not required.
            DefaultBandwidthMeter bandwidthMeterA = new DefaultBandwidthMeter();
            DefaultDataSourceFactory dataSourceFactory
                    = new DefaultDataSourceFactory(content.getContext(), Util.getUserAgent(content.getContext(), content.getContext().getString(R.string.app_name)), bandwidthMeterA);

            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

            MediaSource videoSource = new ExtractorMediaSource(mp4VideoUri,
                    dataSourceFactory, extractorsFactory, null, null);

            //MediaSource loopingSource = new LoopingMediaSource(videoSource);

            if (PreferencesHelper.isCloseGif()) {
                player.prepare(videoSource);

            } else {
                player.prepare(new LoopingMediaSource(videoSource));
            }

            player.setVideoListener(new SimpleExoPlayer.VideoListener() {
                @Override
                public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int h = Math.min((int)((float)size.x * 0.8), (int)((float)height * ((float)content.getHeight() / (float)width)));
                    simpleExoPlayerView.getLayoutParams().width = content.getWidth();
                    simpleExoPlayerView.getLayoutParams().height = h;
                    simpleExoPlayerView.requestLayout();
                }

                @Override
                public void onRenderedFirstFrame(Surface surface) {

                }

                @Override
                public void onVideoDisabled(DecoderCounters counters) {

                }
            });

            player.addListener(new ExoPlayer.EventListener() {

                @Override
                public void onLoadingChanged(boolean isLoading) {
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == ExoPlayer.STATE_ENDED && PreferencesHelper.isCloseGif()) {
                        player.stop();
                        cancelBtn.callOnClick();
                    } else if (playbackState == ExoPlayer.STATE_READY) {
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest) {
                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    Toast.makeText(content.getContext(),
                            "Error load =(",
                            Toast.LENGTH_LONG).show();
                    player.stop();
                    //player.prepare(loopingSource);
                    //player.setPlayWhenReady(true);
                }

                @Override
                public void onPositionDiscontinuity() {
                }
            });
            player.setPlayWhenReady(true);

           /* mediaControls = new MediaController(content.getContext());

            mediaControls.setAnchorView(simpleVideoView);

            simpleVideoView.setMediaController(mediaControls);
            simpleVideoView.setVideoURI(Uri.parse(url));

            simpleVideoView.setOnCompletionListener(myVideoViewCompletionListener);
            simpleVideoView.setOnPreparedListener(MyVideoViewPreparedListener);
            simpleVideoView.setOnErrorListener(myVideoViewErrorListener);
            simpleVideoView.setZOrderOnTop(true);*/

            //simpleVideoView.requestFocus();
        }

        final Dialog dialog = builder.create();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        return dialog;
    }

    @Override
    public void onDestroyView() {
        player.release();
        super.onDestroyView();
    }

    /*MediaPlayer.OnCompletionListener myVideoViewCompletionListener
            = new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer arg0) {

            if (PreferencesHelper.isCloseGif()) {
                cancelBtn.callOnClick();
            } else {
                simpleVideoView.start();
            }
        }
    };

    MediaPlayer.OnPreparedListener MyVideoViewPreparedListener
            = new MediaPlayer.OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer arg0) {
            progressBar.setVisibility(View.INVISIBLE);
            simpleVideoView.start();

        }
    };

    MediaPlayer.OnErrorListener myVideoViewErrorListener
            = new MediaPlayer.OnErrorListener() {

        @Override
        public boolean onError(MediaPlayer arg0, int arg1, int arg2) {
            Toast.makeText(content.getContext(),
                    "Error load =(",
                    Toast.LENGTH_LONG).show();
            return true;
        }
    };*/

    public void setUrl(String url) {
        this.url = url;
    }
}
