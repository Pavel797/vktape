package ru.freshtape.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.R;
import ru.freshtape.vk.VkApiHelper;

public class SettingActivity extends AppCompatActivity {

    private SeekBar scrollOnBtnVoiceSeekBar;
    private ImageButton backBtn;
    private LinearLayout googlePlayBtn, signOutBtn;
    private Button aboutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        backBtn = (ImageButton) findViewById(R.id.backBtn);
        signOutBtn = (LinearLayout) findViewById(R.id.signOutBtn);
        googlePlayBtn = (LinearLayout) findViewById(R.id.googlePlayBtn);
        aboutBtn = (Button) findViewById(R.id.aboutBtn);

        Switch colorScannedPostsSwitch = (Switch) findViewById(R.id.colorScannedPostsSwitch);
        Switch offScannedPostsSwitch = (Switch) findViewById(R.id.offScannedPostsSwitch);
        Switch аiltrationBayansSwitch = (Switch) findViewById(R.id.аiltrationBayansSwitch);
        Switch closeGifSwitch = (Switch) findViewById(R.id.closeGifSwitch);

        CompoundButton.OnCheckedChangeListener checkedListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switch (buttonView.getId()) {
                    case R.id.colorScannedPostsSwitch:
                        PreferencesHelper.setColorScannedPosts(isChecked);
                        break;
                    case R.id.offScannedPostsSwitch:
                        PreferencesHelper.setOffScannedPosts(isChecked);
                        break;
                    case R.id.аiltrationBayansSwitch:
                        PreferencesHelper.setAiltrationBayans(isChecked);
                        if (isChecked)
                            Toast.makeText(getApplicationContext(), R.string.worning_ailtration_bayans, Toast.LENGTH_LONG).show();
                        break;
                    case R.id.scrollOnBtnVoiceSwitch:
                        scrollOnBtnVoiceSeekBar.setEnabled(isChecked);
                        PreferencesHelper.setScrollOnBtnVoice(isChecked);
                        break;
                    case R.id.closeGifSwitch:
                        PreferencesHelper.setCloseGif(isChecked);
                        break;
                }
            }
        };

        closeGifSwitch.setChecked(PreferencesHelper.isCloseGif());
        closeGifSwitch.setOnCheckedChangeListener(checkedListener);

        colorScannedPostsSwitch.setChecked(PreferencesHelper.isColorScannedPosts());
        colorScannedPostsSwitch.setOnCheckedChangeListener(checkedListener);

        offScannedPostsSwitch.setChecked(PreferencesHelper.isOffScannedPosts());
        offScannedPostsSwitch.setOnCheckedChangeListener(checkedListener);

        аiltrationBayansSwitch.setChecked(PreferencesHelper.isAiltrationBayans());
        аiltrationBayansSwitch.setOnCheckedChangeListener(checkedListener);

        Switch scrollOnBtnVoiceSwitch = (Switch) findViewById(R.id.scrollOnBtnVoiceSwitch);
        scrollOnBtnVoiceSwitch.setChecked(PreferencesHelper.isScrollOnBtnVoice());
        scrollOnBtnVoiceSwitch.setOnCheckedChangeListener(checkedListener);

        scrollOnBtnVoiceSeekBar = (SeekBar) findViewById(R.id.scrollOnBtnVoiceSeekBar);
        scrollOnBtnVoiceSeekBar.setMax(PreferencesHelper.MAX_FORCE_SCROLL);
        scrollOnBtnVoiceSeekBar.setProgress(PreferencesHelper.getForceScroll());
        scrollOnBtnVoiceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                PreferencesHelper.setForceScroll(seekBar.getProgress());
            }
        });

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.aboutBtn:
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_web))));
                        break;
                    case R.id.signOutBtn:
                        VkApiHelper.logout();
                        onBackPressed();
                        break;
                    case R.id.backBtn:
                        onBackPressed();
                        break;
                    case R.id.googlePlayBtn:
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=" + getApplicationContext().getString(R.string.app_id_gp))));
                        } catch (android.content.ActivityNotFoundException appPackageName) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(getApplicationContext().getString(R.string.url_app))));
                        }
                        break;
                }
            }
        };

        backBtn.setOnClickListener(listener);
        aboutBtn.setOnClickListener(listener);
        googlePlayBtn.setOnClickListener(listener);
        signOutBtn.setOnClickListener(listener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        scrollOnBtnVoiceSeekBar.setEnabled(PreferencesHelper.isScrollOnBtnVoice());
    }
}
