package ru.freshtape.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import ru.freshtape.model.ModelAnswerGroup;
import ru.freshtape.model.ModelAnswerUser;
import ru.freshtape.model.ModelGroupIntro;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.R;
import ru.freshtape.adapters.ItemsIntroAdapter;
import ru.freshtape.model.CustomItemIntro;
import ru.freshtape.model.ModelUserIntro;
import ru.freshtape.model.Types;
import ru.freshtape.vk.VkApiHelper;

public class ViewFriendsGroupActivity extends AppCompatActivity {

    private ProgressBar pbViewIntroItems;
    private LinearLayoutManager layoutManager;
    private ItemsIntroAdapter adapter;
    private RecyclerView rvViewIntroItems;
    private TextView warningErrorBtn, titleTB;
    private ImageButton backBtn;

    private static long COUNT_GET = 15;
    boolean userScrolled, isLoad, isShow;

    private long backTime, size;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_friends_group);

        backTime = 0;
        size = 0;
        userScrolled = false;
        isLoad = false;
        isShow = true;

        titleTB = (TextView) findViewById(R.id.titleTB);

        backBtn = (ImageButton) findViewById(R.id.backBtn);

        warningErrorBtn = (TextView) findViewById(R.id.warningErrorBtn);
        warningErrorBtn.setVisibility(View.INVISIBLE);

        pbViewIntroItems = (ProgressBar) findViewById(R.id.pbViewIntroItems);
        pbViewIntroItems.setVisibility(View.VISIBLE);

        rvViewIntroItems = (RecyclerView) findViewById(R.id.rvViewIntroItems);
        layoutManager = new LinearLayoutManager(this);
        rvViewIntroItems.setLayoutManager(layoutManager);

        adapter = new ItemsIntroAdapter(this);
        rvViewIntroItems.setAdapter(adapter);
        rvViewIntroItems.addOnScrollListener(new MyScrollListener());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.backBtn:
                        onBackPressed();
                        break;
                }
            }
        };

        backBtn.setOnClickListener(listener);

        Intent intent = getIntent();
        this.type = intent.getIntExtra(PreferencesHelper.TYPE, -1);

        if (type == Types.TYPE_USER) {
            titleTB.setText(R.string.view_user);
        } else {
            titleTB.setText(R.string.view_group);
        }

        loadItems(type, size);
    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        Context context = recyclerView.getContext();
        LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    private void addItems(CustomItemIntro i, boolean isNotify) {
        adapter.addItem(i, isNotify);
    }

    private void loadItems(long type, long offset) {
        if (isLoad)
            return;
        if (type == -1) {
            pbViewIntroItems.setVisibility(View.INVISIBLE);
            warningErrorBtn.setVisibility(View.VISIBLE);
            return;
        }
        isLoad = true;

        if (type == Types.TYPE_USER) {
            loadFriends(offset);
        } else {
            loadGroups(offset);
        }
    }

    private void loadGroups(long offset) {
        VkApiHelper.getMyGroups(COUNT_GET, offset, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                ModelAnswerGroup answer = gson.fromJson(response.json.toString(), ModelAnswerGroup.class);

                boolean isFirst = false;
                if (size == 0)
                    isFirst = true;

                for (ModelGroupIntro i: answer.getResponse().getItems()) {
                    addItems(i, !isFirst);
                }

                if (isFirst)
                    runLayoutAnimation(rvViewIntroItems);

                if (answer.getResponse().getItems().length == 0 || answer.getResponse().getItems().length < COUNT_GET)
                    adapter.setEnd(true);
                size += answer.getResponse().getItems().length;
                pbViewIntroItems.setVisibility(View.INVISIBLE);

                if (answer.getResponse().getItems().length == 0 || answer.getResponse().getItems().length < COUNT_GET)
                    adapter.setEnd(true);

                isLoad = false;
            }

            @Override
            public void onError(VKError error) {
                pbViewIntroItems.setVisibility(View.INVISIBLE);
                isLoad = false;
                super.onError(error);
            }
        });
    }

    private void loadFriends(long offset) {
        VkApiHelper.getMyFriends(COUNT_GET, offset, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                ModelAnswerUser answer = gson.fromJson(response.json.toString(), ModelAnswerUser.class);

                boolean isFirst = false;
                if (size == 0)
                    isFirst = true;

                for (ModelUserIntro i: answer.getResponse().getItems()) {
                    addItems(i, !isFirst);
                }

                if (isFirst)
                    runLayoutAnimation(rvViewIntroItems);

                if (answer.getResponse().getItems().length == 0 || answer.getResponse().getItems().length < COUNT_GET)
                    adapter.setEnd(true);
                size += answer.getResponse().getItems().length;
                pbViewIntroItems.setVisibility(View.INVISIBLE);

                if (answer.getResponse().getItems().length == 0 || answer.getResponse().getItems().length < COUNT_GET)
                    adapter.setEnd(true);

                isLoad = false;
            }

            @Override
            public void onError(VKError error) {
                pbViewIntroItems.setVisibility(View.INVISIBLE);
                isLoad = false;
                super.onError(error);
            }
        });
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                loadItems(type, size);
            }
        }
    }
}
