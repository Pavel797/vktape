package ru.freshtape.activities;

import android.animation.Animator;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import ru.freshtape.R;
import ru.freshtape.adapters.PostAdapter;
import ru.freshtape.db.DBHelper;
import ru.freshtape.model.CustomItemIntro;
import ru.freshtape.model.CustomItemPost;
import ru.freshtape.model.ModelAnswerPost;
import ru.freshtape.model.ModelGroup;
import ru.freshtape.model.ModelGroupIntro;
import ru.freshtape.model.ModelPost;
import ru.freshtape.model.ModelPostWithAuthor;
import ru.freshtape.model.ModelUser;
import ru.freshtape.model.ModelUserIntro;
import ru.freshtape.model.Types;
import ru.freshtape.vk.VkApiHelper;
import ru.freshtape.vk.VkErrorHelper;

public class ViewWallActivity extends AppCompatActivity {

    private ProgressBar pbViewWall;
    private LinearLayoutManager layoutManager;
    private PostAdapter adapter;
    private RecyclerView rvViewWall;
    private FloatingActionButton fabAdd;
    private Toolbar viewTB;
    private DBHelper dbHelper;
    private TextView warningErrorBtn;
    private ImageButton backBtn;

    private static long COUNT_GET = 15;
    boolean userScrolled, isLoad, isShow;

    private long size, from_id; // backTime
    private CustomItemIntro mainItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_wall);

        //backTime = 0;

            size = 0;
            userScrolled = false;
            isLoad = false;
        isShow = true;

        dbHelper = new DBHelper(getApplication());

        fabAdd = (FloatingActionButton) findViewById(R.id.fabAdd);
        viewTB = (Toolbar) findViewById(R.id.viewTB);

        warningErrorBtn = (TextView) findViewById(R.id.warningErrorBtn);
        warningErrorBtn.setVisibility(View.INVISIBLE);

        pbViewWall = (ProgressBar) findViewById(R.id.pbViewWall);
        pbViewWall.setVisibility(View.VISIBLE);

        rvViewWall = (RecyclerView) findViewById(R.id.rvViewPost);
        layoutManager = new LinearLayoutManager(this);
        rvViewWall.setLayoutManager(layoutManager);

        adapter = new PostAdapter(this);
        rvViewWall.setAdapter(adapter);
        rvViewWall.addOnScrollListener(new MyScrollListener());

        backBtn = (ImageButton) findViewById(R.id.backBtn);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.backBtn:
                        onBackPressed();
                        break;
                    case R.id.fabAdd:
                        addDelFab();
                        break;
                }
            }
        };

        fabAdd.setOnClickListener(listener);
        backBtn.setOnClickListener(listener);

        Intent intent = getIntent();
        switch (intent.getIntExtra(DBHelper.KEY_TYPE_INT, -1)) {
            case Types.TYPE_USER:
                ModelUserIntro modelUser = new ModelUserIntro();
                modelUser.setId(intent.getLongExtra(DBHelper.KEY_VK_ID, -1));
                modelUser.setFirst_name(intent.getStringExtra(DBHelper.KEY_NAME));
                modelUser.setType(intent.getStringExtra(DBHelper.KEY_TYPE));
                modelUser.setDomain(intent.getStringExtra(DBHelper.KEY_SHORT_NAME));
                modelUser.setPhoto_50(intent.getStringExtra(DBHelper.KEY_PHOTO_50));
                modelUser.setPhoto_100(intent.getStringExtra(DBHelper.KEY_PHOTO_100));
                modelUser.setPhoto_200(intent.getStringExtra(DBHelper.KEY_PHOTO_200));

                from_id = modelUser.getId();

                mainItem = modelUser;
                break;
            case Types.TYPE_GROUP:
                ModelGroupIntro modelGroup = new ModelGroupIntro();
                modelGroup.setId(intent.getLongExtra(DBHelper.KEY_VK_ID, -1));
                modelGroup.setName(intent.getStringExtra(DBHelper.KEY_NAME));
                modelGroup.setType(intent.getStringExtra(DBHelper.KEY_TYPE));
                modelGroup.setScreen_name(intent.getStringExtra(DBHelper.KEY_SHORT_NAME));
                modelGroup.setPhoto_50(intent.getStringExtra(DBHelper.KEY_PHOTO_50));
                modelGroup.setPhoto_100(intent.getStringExtra(DBHelper.KEY_PHOTO_100));
                modelGroup.setPhoto_200(intent.getStringExtra(DBHelper.KEY_PHOTO_200));

                from_id = modelGroup.getId();

                mainItem = modelGroup;
                break;
            default:
                throw new NullPointerException("Activity is not put all fields extra!");
        }

        loadWall(from_id, size);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (dbHelper.findItemInFavorites(dbHelper.getReadableDatabase(), from_id)) {
            fabAdd.setImageDrawable(getResources().getDrawable(R.drawable.ic_remove));
            fabAdd.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.rad)));
        } else {
            fabAdd.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.add)));
            fabAdd.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
        }
    }

    public void addItems(CustomItemPost i) {
        pbViewWall.setVisibility(View.INVISIBLE);
        adapter.addItem(i);
    }

    private void addDelFab() {
        dbHelper.addItem(dbHelper, mainItem,
                new DBHelper.CallbackAddFavorites() {
                    @Override
                    public void del() {
                        fabAdd.animate().rotationBy(180f).setInterpolator(new AccelerateInterpolator(8)).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                fabAdd.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.add)));
                                fabAdd.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }

                    @Override
                    public void add() {
                        fabAdd.animate().rotationBy(-180f).setInterpolator(new AccelerateInterpolator(8)).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                fabAdd.setImageDrawable(getResources().getDrawable(R.drawable.ic_remove));
                                fabAdd.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.rad)));
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }
                });
    }

    private void loadWall(long from_id, long offset) {
        if (isLoad)
            return;
        if (from_id == 0) {
            pbViewWall.setVisibility(View.INVISIBLE);
            warningErrorBtn.setVisibility(View.VISIBLE);
        }
        isLoad = true;
        VkApiHelper.wallGet(from_id, COUNT_GET, offset, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                ModelAnswerPost answer = gson.fromJson(response.json.toString(), ModelAnswerPost.class);

                ModelGroup modelGroup[] = null;
                ModelUser modelUsers[] = null;

                if (answer.getResponse().getGroups() != null && answer.getResponse().getGroups().length > 0) {
                    modelGroup = new ModelGroup[answer.getResponse().getGroups().length];
                    for (int i = 0; i < answer.getResponse().getGroups().length; i++) {
                        modelGroup[i] = new ModelGroup();
                        modelGroup[i].setIs_author(answer.getResponse().getGroups()[i].getIs_author());
                        modelGroup[i].setInfo(answer.getResponse().getGroups()[i]);
                    }
                }
                if (answer.getResponse().getProfiles() != null && answer.getResponse().getProfiles().length > 0) {
                    modelUsers = new ModelUser[answer.getResponse().getProfiles().length];
                    for (int i = 0; i < answer.getResponse().getProfiles().length; i++) {
                        modelUsers[i] = new ModelUser();
                        modelUsers[i].setIs_author(answer.getResponse().getProfiles()[i].getIs_author());
                        modelUsers[i].setInfo(answer.getResponse().getProfiles()[i]);
                    }
                }

                for (ModelPost i: answer.getResponse().getItems()) {
                    ModelPostWithAuthor model = new ModelPostWithAuthor();
                    model.setProfile(modelUsers);
                    model.setGroup(modelGroup);
                    model.setWall(i);
                    addItems(model);
                }

                size += answer.getResponse().getItems().length;
                if (size == 0) {
                    fabAdd.setVisibility(View.GONE);
                } else {
                    fabAdd.setVisibility(View.VISIBLE);
                }
                if (answer.getResponse().getItems().length == 0 || answer.getResponse().getItems().length < COUNT_GET)
                    adapter.setEnd(true);
                isLoad = false;
            }

            @Override
            public void onError(VKError error) {
                pbViewWall.setVisibility(View.INVISIBLE);
                warningErrorBtn.setVisibility(View.VISIBLE);
                VkErrorHelper.error(error, getApplicationContext());
                super.onError(error);
                isLoad = false;
            }
        });
    }

    public void hideViews() {
        if (!isShow)
            return;
        viewTB.animate().translationY(-viewTB.getHeight()).setInterpolator(new AccelerateInterpolator(2));
        fabAdd.hide();

        //FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFabButton.getLayoutParams();
        //int fabBottomMargin = lp.bottomMargin;
        //mFabButton.animate().translationY(mFabButton.getHeight()+fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
        isShow = false;
    }

    public void showViews() {
        if (isShow)
            return;
        viewTB.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
        fabAdd.show();

        //mFabButton.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
        isShow = true;
    }

    private class MyScrollListener extends RecyclerView.OnScrollListener {

        private static final int HIDE_THRESHOLD = 100;
        private int scrolledDistance = 0;
        private boolean controlsVisible = true;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                userScrolled = true;
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

            // подгрузка
            if (userScrolled && (visibleItemCount + pastVisiblesItems + 3) >= totalItemCount && dy > 0) {
                userScrolled = false;
                loadWall(from_id, size);
            }

            // видимость/невидемость
            if (pastVisiblesItems == 0) {
                if (!controlsVisible) {
                    showViews();
                    controlsVisible = true;
                }
            } else {
                if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                    hideViews();
                    controlsVisible = false;
                    scrolledDistance = 0;
                } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
                    showViews();
                    controlsVisible = true;
                    scrolledDistance = 0;
                }
            }

            if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
                scrolledDistance += dy;
            }
        }
    }
}
