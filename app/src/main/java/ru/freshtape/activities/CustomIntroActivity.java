package ru.freshtape.activities;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro2;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.R;
import ru.freshtape.fragments.SlideFragment;
import ru.freshtape.vk.VkApiHelper;
import ru.freshtape.vk.VkErrorHelper;

/**
 * Created by Павел on 07.05.2017.
 */

public class CustomIntroActivity extends AppIntro2 {

    @Override
    public void init(Bundle savedInstanceState) {

        // Здесь указываем количество слайдов, например нам нужно 3
        addSlide(SlideFragment.newInstance(R.layout.intro_1));
        addSlide(SlideFragment.newInstance(R.layout.intro_2));
        addSlide(SlideFragment.newInstance(R.layout.intro_3));
    }

    @Override
    public void onBackPressed() {}

    @Override
    public void onNextPressed() {
        // Do something here
    }

    @Override
    public void onDonePressed() {
        // уже не первый запуск
        PreferencesHelper.setFirstApplicationLaunch();
        VKSdk.login(this, VkApiHelper.scope);
    }

    @Override
    public void onSlideChanged() {
        // Do something here
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                PreferencesHelper.setUserId(Long.parseLong(res.userId));
                finish();
            }

            @Override
            public void onError(VKError error) {
                VkErrorHelper.error(error, getApplicationContext());
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
