package ru.freshtape.activities;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.AppCompatImageView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.vk.sdk.VKSdk;
import com.yandex.metrica.YandexMetrica;

import ru.freshtape.fragments.FindFragment;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.R;
import ru.freshtape.fragments.FavoritesFragment;
import ru.freshtape.fragments.TapeFragment;

public class MainActivity extends Activity {

    private BottomNavigationViewEx  mainBNV;
    private TextView statusTV;
    private FragmentTransaction fragmentTransaction;
    private FavoritesFragment favoritesFragment;
    private FindFragment findFragment;
    private TapeFragment tapeFragment;
    private Toolbar mainTB;
    private AppCompatImageView goToUp, prefBtn;

    private boolean isShow;

    private boolean isScrollOnBtnVoice, isTapeFragment;
    private int forceScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        favoritesFragment = new FavoritesFragment();
        findFragment = new FindFragment();
        tapeFragment = new TapeFragment();

        isShow = true;

        statusTV = (TextView) findViewById(R.id.statusTV);
        mainTB = (Toolbar) findViewById(R.id.mainTB);
        goToUp = (AppCompatImageView) findViewById(R.id.goToUp);
        goToUp.setVisibility(View.INVISIBLE);

        prefBtn = (AppCompatImageView) findViewById(R.id.prefBtn);

        mainBNV = (BottomNavigationViewEx) findViewById(R.id.mainBNV);
        //mainBNV.enableAnimation(false);
        //mainBNV.enableShiftingMode(false);
        //mainBNV.enableItemShiftingMode(false);
        mainBNV.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_find:
                        onFindFragment();
                        return true;
                    case R.id.action_tape:
                        onТapeFragment();
                        return true;
                    case R.id.action_favorites:
                        onFavoritesFragment();
                        return true;
                    default:
                        return false;
                }
            }
        });

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.prefBtn:
                        Intent intent = new Intent(getApplication(), SettingActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.goToUp:
                        checkGoToUp();
                        break;
                }
            }
        };

        goToUp.setOnClickListener(listener);
        prefBtn.setOnClickListener(listener);

        // начинаем с ленты
        mainBNV.getMenu().findItem(R.id.action_tape).setChecked(true);
        onТapeFragment();
    }

    private void swipeAnim(final TextView tv) {
        final Animation animScale = AnimationUtils.loadAnimation(this, R.anim.scale);
        animScale.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation anim = AnimationUtils.loadAnimation(getApplication(), R.anim.translate);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        tv.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                tv.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tv.startAnimation(animScale);
    }

    public void onFindFragment() {
        showViews();
        mainBNV.getMenu().findItem(R.id.action_find).setChecked(true);
        fragmentTransaction = getFragmentManager().beginTransaction();
        statusTV.setText(getApplicationContext().getString(R.string.find));
        fragmentTransaction.replace(R.id.mainFragmentContainer, findFragment).commit();
        isTapeFragment = false;
        swipeAnim(statusTV);
        goToUp.setVisibility(View.INVISIBLE);
    }

    private void onТapeFragment() {
        showViews();
        findFragment.clear();
        favoritesFragment.clear();
        mainBNV.getMenu().findItem(R.id.action_tape).setChecked(true);
        fragmentTransaction = getFragmentManager().beginTransaction();
        statusTV.setText(getApplicationContext().getString(R.string.tape));
        fragmentTransaction.replace(R.id.mainFragmentContainer, tapeFragment).commit();
        isTapeFragment = true;
        swipeAnim(statusTV);
        goToUp.setVisibility(View.INVISIBLE);
    }

    private void onFavoritesFragment() {
        showViews();
        mainBNV.getMenu().findItem(R.id.action_favorites).setChecked(true);
        fragmentTransaction = getFragmentManager().beginTransaction();
        statusTV.setText(getApplicationContext().getString(R.string.favorits));
        fragmentTransaction.replace(R.id.mainFragmentContainer, favoritesFragment).commit();
        isTapeFragment = false;
        swipeAnim(statusTV);
        goToUp.setVisibility(View.INVISIBLE);
    }

    public void hideViews() {
        if (!isShow)
            return;
        mainTB.animate().translationY(-mainTB.getHeight()).setInterpolator(new AccelerateInterpolator(2));
        mainBNV.animate().translationY(mainBNV.getY() + mainBNV.getHeight()).setInterpolator(new AccelerateInterpolator(2));

        //FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFabButton.getLayoutParams();
        //int fabBottomMargin = lp.bottomMargin;
        //mFabButton.animate().translationY(mFabButton.getHeight()+fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
        isShow = false;
    }

    public void showViews() {
        if (isShow)
            return;
        mainTB.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
        mainBNV.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));

        //mFabButton.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
        isShow = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        YandexMetrica.getReporter(getApplicationContext(), PreferencesHelper.YM_API_KEY).onResumeSession();
    }

    @Override
    protected void onPause() {
        YandexMetrica.getReporter(getApplicationContext(), PreferencesHelper.YM_API_KEY).onPauseSession();
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (isScrollOnBtnVoice && isTapeFragment) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_VOLUME_UP:
                    tapeFragment.scrollUP(-forceScroll);
                    return true;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    tapeFragment.scrollDOWN(forceScroll);
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isScrollOnBtnVoice = PreferencesHelper.isScrollOnBtnVoice();
        forceScroll = PreferencesHelper.getForceScroll();

        // проверяем авторизацию
        if (PreferencesHelper.isFirstApplicationLaunch() || !VKSdk.isLoggedIn()) {
            Intent intent = new Intent(MainActivity.this, CustomIntroActivity.class);
            startActivity(intent);
        }
    }

    public void setGoToUpVisibility(int visibility) {
        goToUp.setVisibility(visibility);
    }

    private void checkGoToUp() {
        goToUp.setVisibility(View.INVISIBLE);
        tapeFragment.goToUp();
    }
}
