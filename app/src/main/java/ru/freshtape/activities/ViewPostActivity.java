package ru.freshtape.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import ru.freshtape.model.CustomItemPost;
import ru.freshtape.preferences.PreferencesHelper;
import ru.freshtape.R;
import ru.freshtape.adapters.PostAdapter;
import ru.freshtape.model.ModelGetPostWithAuthor;
import ru.freshtape.vk.VkApiHelper;
import ru.freshtape.vk.VkErrorHelper;

public class ViewPostActivity extends AppCompatActivity {


    private RecyclerView rvViewPost;
    private PostAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ProgressBar pbViewPost;
    private TextView warningErrorBtn;
    private ImageButton backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        backBtn = (ImageButton) findViewById(R.id.backBtn);

        warningErrorBtn = (TextView) findViewById(R.id.warningErrorBtn);
        warningErrorBtn.setVisibility(View.INVISIBLE);

        pbViewPost = (ProgressBar) findViewById(R.id.pbViewPost);
        pbViewPost.setVisibility(View.VISIBLE);

        rvViewPost = (RecyclerView) findViewById(R.id.rvViewPost);
        layoutManager = new LinearLayoutManager(this);
        rvViewPost.setLayoutManager(layoutManager);

        adapter = new PostAdapter(this);
        adapter.setAllView(true);
        adapter.setEnd(true);
        rvViewPost.setAdapter(adapter);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.backBtn:
                        onBackPressed();
                        break;
                    /*case R.id.fabAddFavorites:
                        Toast.makeText(getApplicationContext(), "Тык", Toast.LENGTH_SHORT).show();
                        break;*/
                }
            }
        };

        backBtn.setOnClickListener(listener);

        Intent intent = getIntent();
        loadPost(intent.getLongExtra(PreferencesHelper.FROM_ID, 0), intent.getLongExtra(PreferencesHelper.ID_POST, 0));
    }

    public void addItems(CustomItemPost i) {
        pbViewPost.setVisibility(View.INVISIBLE);
        adapter.addItem(i);
    }

    public void loadPost(Long from_id, Long id_post) {
        if (from_id == 0 || id_post == 0) {
            warningErrorBtn.setVisibility(View.VISIBLE);
            return;
        }
        VkApiHelper.wallGetOnePost(from_id, id_post, new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                ModelGetPostWithAuthor answer = gson.fromJson(response.json.toString(), ModelGetPostWithAuthor.class);

                if (answer.getResponse().getWall() == null) {

                }

                addItems(answer.getResponse());
            }

            @Override
            public void onError(VKError error) {
                warningErrorBtn.setVisibility(View.VISIBLE);
                VkErrorHelper.error(error, getApplicationContext());
                super.onError(error);
            }
        });
    }
}
