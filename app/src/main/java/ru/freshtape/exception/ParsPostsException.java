package ru.freshtape.exception;

/**
 * Created by Павел on 11.05.2017.
 */

public class ParsPostsException extends Exception {

    //public ParsPostsException() {}

    public ParsPostsException(String msg) {
        super(msg);
    }
}
