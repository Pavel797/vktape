package ru.freshtape;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ru.freshtape.model.SmallModelPost;
import ru.freshtape.vk.ViewedSet;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private ViewedSet set;

    @Before
    public void setUp() throws Exception {
        this.set = new ViewedSet(null);
        for (int i = 10; i >= 1; i--) {
            System.out.println(
                    set.getNext(new SmallModelPost(1, 1, i)).getId()
            );
        }
    }

    @Test
    public void addition_isCorrect() throws Exception {

        for (int i = 15; i >= 1; i--) {
            System.out.println(
                    set.getNext(new SmallModelPost(1, 1, i)).getId() + " " + i
            );
        }
    }

    @After
    public void tearDown() throws Exception {
        this.set.clear();
        this.set = null;
    }
}